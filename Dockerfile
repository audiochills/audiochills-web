FROM node:14
WORKDIR /app
EXPOSE 5000

RUN yarn global add serve

COPY package.json package.json
COPY yarn.lock yarn.lock
RUN yarn

COPY . .
RUN yarn build

CMD ["serve", "--single", "--listen", "5000", "build/"]
