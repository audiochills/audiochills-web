terraform {
  backend "http" {}
  required_providers {
    docker = {
      source  = "kreuzwerker/docker"
      version = "2.11.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.0.2"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "2.0.2"
    }
    random = {
      source  = "hashicorp/random"
      version = "3.0.1"
    }
    htpasswd = {
      source  = "loafoe/htpasswd"
      version = "0.9.0"
    }
  }
}

resource "kubernetes_namespace" "namespace" {
  metadata {
    name   = var.kube_namespace
    labels = merge(var.kube_annotations, { name = var.kube_namespace })
  }
}

resource "kubernetes_secret" "image_pull_secrets" {
  metadata {
    name      = "gitlab-registry"
    namespace = kubernetes_namespace.namespace.metadata.0.name
  }
  data = {
    ".dockerconfigjson" = templatefile("${path.module}/dockerconfig.json", {
      docker_registry_name     = var.docker_registry_name
      docker_registry_username = var.docker_registry_username
      docker_registry_password = var.docker_registry_password
    })
  }
  type = "kubernetes.io/dockerconfigjson"
}

provider "docker" {
  registry_auth {
    address = var.docker_registry_name
  }
}

resource "docker_registry_image" "node" {
  name = "${var.docker_registry_name}/${var.docker_registry_image_repository}/node:${var.docker_registry_image_tag}"
  build {
    context  = ".."
    build_id = uuid()
  }
}

resource "random_password" "node_auth_salt" {
  length = 8
}

resource "htpasswd_password" "node_auth_hash" {
  password = var.node_auth_password
  salt     = random_password.node_auth_salt.result
}

resource "kubernetes_secret" "node_auth_secret" {
  metadata {
    name      = "node-auth"
    namespace = kubernetes_namespace.namespace.metadata.0.name
  }
  data = {
    auth = "${var.node_auth_username}:${htpasswd_password.node_auth_hash.apr1}"
  }
}

resource "helm_release" "node" {
  name       = "node"
  repository = "https://charts.bitnami.com/bitnami"
  chart      = "node"
  namespace  = kubernetes_namespace.namespace.metadata.0.name
  values = [
    templatefile("${path.module}/node.values.yaml", {
      node_env          = var.node_env
      node_domain       = var.node_domain
      node_auth_enabled = var.node_auth_enabled
      node_auth_type    = "basic"
      node_auth_secret  = kubernetes_secret.node_auth_secret.metadata.0.name
      kube_annotations  = var.kube_annotations
    })
  ]

  set {
    name  = "image.registry"
    value = var.docker_registry_name
  }
  set {
    name  = "image.repository"
    value = "${var.docker_registry_image_repository}/node@sha256"
  }
  set {
    name  = "image.tag"
    value = trimprefix(docker_registry_image.node.sha256_digest, "sha256:")
  }
  set {
    name  = "image.pullPolicy"
    value = "Always"
  }
  set {
    name  = "image.pullSecrets[0]"
    value = kubernetes_secret.image_pull_secrets.metadata.0.name
  }
}

output "node_url" {
  value = "https://${var.node_domain}"
}
