import i18n from 'i18next';
import Locize from 'i18next-locize-backend';
import { promisify } from 'es6-promisify';

const locize = new Locize({
  allowedAddOrUpdateHosts: ['www.development.audiochills.art'],
  apiKey: process.env.REACT_APP_LOCIZE_API_KEY!,
  projectId: process.env.REACT_APP_LOCIZE_PROJECT_ID!,
  referenceLng: 'en',
  version: process.env.REACT_APP_LOCIZE_VERSION,
});

if (process.env.NODE_ENV !== 'test') i18n.use(locize);

export const initialized = promisify((cb) => i18n.init({
  fallbackLng: 'en',
  interpolation: {
    escapeValue: false, // react already safes from xss
  },
  // react: { wait: true },
  saveMissing: true,
}, cb))();

export default i18n;
