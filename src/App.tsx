import React from 'react';
import { I18nextProvider } from 'react-i18next';
import {
  Route, RouteProps, Router, Switch,
} from 'react-router-dom';
import { ParallaxProvider } from 'react-scroll-parallax';
import { ErrorBoundary } from 'react-error-boundary';
import i18n from './i18n';
import history from './history';
import Layout, { LoadingPage, NotFoundPage, ErrorPage } from './services/Layout';
import Profile, { Modals as ProfileModals } from './services/Profile';
import AudioPlayer from './services/AudioPlayer';
import PostCreation from './services/PostCreation';
import Feed from './services/Feed';
import Onboarding from './services/Onboarding';
import { initAmplitude, initGA } from './analytics';
import Law from './services/Law';
import Explore from './services/Explore';

export type AppProps = {

};

initGA();
initAmplitude();

function renderRoutes(children: JSX.Element[]) {
  return React.Children.map(children, (child) => (
    React.cloneElement(child, {
      render: (props: RouteProps) => (
        <ErrorBoundary
          key={child.props.path}
          fallbackRender={({ error }) => <ErrorPage error={error} />}
        >
          {child.props.render(props)}
        </ErrorBoundary>
      ),
    })
  ));
}

export const App: React.FC<AppProps> = () => (

  <div className="d-flex flex-row justify-content-center">
    <div style={{ maxWidth: '600px', minHeight: '100vh', width: '100vw' }} className="shadow">
      <React.Suspense fallback={<LoadingPage />}>

        <ParallaxProvider>
          <I18nextProvider i18n={i18n}>
            <Router history={history}>
              <Layout>
                <React.Suspense fallback={<LoadingPage />}>
                  <ProfileModals />
                  <AudioPlayer />
                  <Switch>
                    {renderRoutes(Explore())}
                    {renderRoutes(Profile())}
                    {renderRoutes(Feed())}
                    {/* {renderRoutes(Notification())} */}
                    {renderRoutes(Onboarding())}
                    {renderRoutes(PostCreation())}
                    {renderRoutes(Law())}
                    <Route render={() => <NotFoundPage />} />
                  </Switch>
                </React.Suspense>
              </Layout>
            </Router>
          </I18nextProvider>
        </ParallaxProvider>
      </React.Suspense>
    </div>
  </div>

);
export default App;
