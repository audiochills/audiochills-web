import {
  of, Observable, fromEvent, concat, from,
} from 'rxjs';
import {
  map, mapTo, mergeMap, shareReplay,
} from 'rxjs/operators';
import io from 'socket.io-client';
import feathers, { Application as Feathers, ServiceAddons } from '@feathersjs/feathers';
import socketio from '@feathersjs/socketio-client';
import rx from 'feathers-reactive';
import { AttributeTypes } from '@audiochills/api-social/src/attributes';
import { AdapterService } from '@feathersjs/adapter-commons';
import { AuthenticationResult } from '@feathersjs/authentication';
import authentication from '@feathersjs/authentication-client';
import { stripSlashes } from '@feathersjs/commons';
import PQueue from 'p-queue';
import keycloak, { keycloakInit } from '../keycloak';

declare module '@feathersjs/feathers' {
  interface ReactiveService<T> {
    find(params?: Params): Observable<Paginated<T>>;
  }
}

declare module '@feathersjs/authentication-client' {
  interface AuthenticationClient {
    result(): Observable<AuthenticationResult | false>;
  }
}

export type ServiceTypes = {
  [Property in keyof AttributeTypes]:
  AdapterService<AttributeTypes[Property]> & ServiceAddons<AttributeTypes[Property]>
};

export type Application = Feathers<ServiceTypes>;

/**
 * A mirror of the feathers api-social app.
 */
export const social = feathers<ServiceTypes>();

social.configure(socketio(io(process.env.REACT_APP_API_SOCIAL_URL!, { autoConnect: false })));
social.configure(rx({ idField: 'id' }));
social.configure(authentication({
  jwtStrategy: 'oidc',
  storageKey: 'auth',
}));

const init = async () => {
  await social.io.connect();
  await keycloakInit;
  if (!keycloak) return false;
  keycloak.onAuthLogout = ((onAuthLogout) => () => {
    if (onAuthLogout) onAuthLogout();
    social.logout();
  })(keycloak.onAuthLogout);

  if (!keycloak.authenticated) return false;
  await keycloak.updateToken(5).catch(() => keycloak.login({ prompt: 'none' }));
  return await social.authenticate({
    accessToken: keycloak.token,
    strategy: 'oidc',
    updateEntity: true,
  });
};
export const socialInit = init();

const authenticationObservable = concat(from(socialInit), fromEvent<AuthenticationResult>(social, 'logout').pipe(mapTo<AuthenticationResult, false>(false)));
const mappedAuthenticationObservable = authenticationObservable.pipe(mergeMap((auth) => (!auth ? of(auth) : social.service('profile').watch().get(auth.profile.id).pipe(map((profile) => Object.assign(auth, { profile } as AuthenticationResult))))), shareReplay(1));

social.authentication.result = () => mappedAuthenticationObservable;

const update = async () => {
  await socialInit;
  if (!keycloak) return false;
  if (!keycloak.authenticated) return false;
  const updated = await keycloak.updateToken(5).catch(() => keycloak.login({ prompt: 'none' }));
  return updated && await social.authenticate({
    accessToken: keycloak.token,
    strategy: 'oidc',
  });
};
const queue = new PQueue({ concurrency: 1 });

social.hooks({
  before: async ({ app: { authentication: service }, path }) => {
    if (stripSlashes(service.options.path) !== path && keycloak && keycloak.authenticated) {
      await queue.add(update);
    }
    // await promisify((cb) => setTimeout(cb, 1000))();
  },
});

// Convert get() function for adding more search capabilities
social.hooks({
  before: {
    async get(context) {
      if (!context.id) return context;
      const [slug, ...rest] = context.id.toString().split(':');
      if (!rest.length) return context;
      const res = await context.service.find({ query: { [slug]: rest.join(':') } });
      const data = Array.isArray(res) ? res : res.data;
      if (!data.length) throw new Error('Not found');
      [context.result] = data;
      return context;
    },
  },
});

export default social;

if (window.location.hostname !== 'www.audiochills.art') Object.assign(window, { social });

// eslint-disable-next-line @typescript-eslint/no-throw-literal
export const Pending: React.FC<{}> = () => { throw new Promise(() => undefined); };
