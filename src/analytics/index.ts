import amplitude from 'amplitude-js';
import ReactGA from 'react-ga';
import social from '../apis/social';

const instance = process.env.REACT_APP_AMPLITUDE_INSTANCE;

export const initAmplitude = async () => {
  try {
    const authentication = await social.authentication.result().toPromise();
    if (authentication) {
      amplitude.getInstance(instance)
        .init(process.env.REACT_APP_AMPLITUDE_API_KEY ?? '', authentication?.profile?.id, { includeReferrer: true, includeUtm: true });
    } else {
      amplitude.getInstance(instance)
        .init(process.env.REACT_APP_AMPLITUDE_API_KEY ?? '', undefined, { includeReferrer: true, includeUtm: true });
    }
  } catch (e) {
    amplitude.getInstance(instance)
      .init(process.env.REACT_APP_AMPLITUDE_API_KEY ?? '', undefined, { includeReferrer: true, includeUtm: true });
  }
};

export const unsetUserId = async () => {
  amplitude.getInstance(process.env.REACT_APP_AMPLITUDE_INSTANCE).setUserId(null);
};

export const logEvent = async (event: string, data?: any) => {
  amplitude.getInstance(process.env.REACT_APP_AMPLITUDE_INSTANCE).logEvent(event, data);
};

export const eventNames = {
  BECOME_CREATOR: 'Become Creator', // done
  COMMENT_MADE: 'Comment Made', // done
  FOLLOW_CREATOR: 'Follow Creator', // done
  PAYMENT_FAILED: 'Payment failed',
  PLAY_CREATION: 'Play Creation', // done
  PUBLISHED_CREATION: 'Published Creation', // done
  SEND_TIP: 'Send Tip Successful', // done
  STARTED_POST_CREATION: 'Started Post Creation', // done
  START_CREATOR_JOURNEY: 'Starts Creator Journey', // done
  SUBSCRIPTION_SUCCESSFUL: 'Subscription Successful', // done
  UNLOCK_CREATION: 'Unlock Successful', // done
};

export const initGA = () => {
  ReactGA.initialize(process.env.REACT_APP_GA ?? '');
  ReactGA.pageview(window.location.pathname + window.location.search);
};

export default initAmplitude;
