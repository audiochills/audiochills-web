import type { KeycloakInstance, KeycloakPromise, KeycloakError } from 'keycloak-js';

declare global {
  interface Window {
    keycloak: KeycloakInstance
    keycloakInit: KeycloakPromise<boolean, KeycloakError>
  }
} export const { keycloak, keycloakInit } = window;
// import Keycloak from 'keycloak-js';
// export const keycloak = Keycloak({
//   clientId: process.env.REACT_APP_KEYCLOAK_CLIENT_ID!,
//   realm: process.env.REACT_APP_KEYCLOAK_REALM!,
//   url: process.env.REACT_APP_KEYCLOAK_URL!,
// });

// const createKeycloakInit = async () => {
//   const authenticated = await keycloak.init({
//     onLoad: 'check-sso',
//   });
//   return authenticated;
// };
// export const keycloakInit = createKeycloakInit();

// if (process.env.NODE_ENV !== 'production') Object.assign(window, { keycloak });

export default keycloak;
