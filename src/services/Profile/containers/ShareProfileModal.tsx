import React from 'react';
import {
  Col, Row, Modal,
} from 'react-bootstrap';
import { useModalManager } from '@vlzh/react-modal-manager';
import {
  FacebookShareButton,
  RedditShareButton,
  TwitterShareButton,
  FacebookIcon,
  RedditIcon,
  TwitterIcon,
} from 'react-share';

import acLogo from '../images/acLogo.svg';

export type ShareProfileModalProps = {
};

export type ShareProfileModalParams = null | {
  shareUrl?: string
};

export const ShareProfileModal: React.FC<ShareProfileModalParams> = () => {
  const {
    isOpen, closeModal, getParams,
  } = useModalManager('ShareProfileModal');

  const params = getParams() as ShareProfileModalParams;
  const { shareUrl } = params || { shareUrl: undefined };
  // const [post] = useObservable(
  //   () => (typeof postOverride !== 'undefined'
  // ? of(postOverride) : social.service('post').watch().get(`${postId}`)),
  //   [postId, postOverride],
  // );

  return (
    <>
      <Modal centered show={isOpen()} onHide={() => closeModal()} style={{ zIndex: 9000 }}>

        {shareUrl && (
        <>
          <Modal.Header closeButton className="overflow-hidden" style={{ background: `no-repeat center/100% url(${acLogo})`, height: '120px' }} />
          <Modal.Body className="pt-0">
            <Row className="no-gutters">

              <Col className="m-2">
                <p className="font-weight-bold mb-0 text-center">
                  {/* {post?.author?.username} */}
                  Share
                </p>
              </Col>

            </Row>

            <div className="d-flex justify-content-around h-100 w-100 py-3">
              <TwitterShareButton url={shareUrl ?? ''} className="mr-2">
                <TwitterIcon size={42} />
              </TwitterShareButton>

              <FacebookShareButton url={shareUrl ?? ''} className="mr-2">
                <FacebookIcon size={42} />
              </FacebookShareButton>

              <RedditShareButton url={shareUrl ?? ''} className="mr-2">
                <RedditIcon size={42} />
              </RedditShareButton>

            </div>

          </Modal.Body>
        </>
        )}

      </Modal>
    </>
  );
};

export default ShareProfileModal;
