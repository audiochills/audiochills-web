import React from 'react';
import { useTranslation } from 'react-i18next';
import {
  Col, Row,
} from 'react-bootstrap';

import { CommentAttributes } from '@audiochills/api-social/src/attributes';
import Moment from 'react-moment';
import { useToggle } from 'react-use';
import Truncate from 'react-truncate';
import moment from 'moment';
import { useObservable } from 'react-use-observable';
import social, { Pending } from '../../../apis/social';
import avatar from '../images/avatar-placeholder.png';
import { ReactComponent as VerifiedSvg } from '../images/verified.svg';

export type SubcommentItemProps = {
  subcommentId: string
  subcommentOverride?: CommentAttributes
  disableLine?: boolean
};

export const SubcommentItem:
React.FC<SubcommentItemProps> = ({ subcommentOverride, disableLine = false }) => {
  const { t } = useTranslation('SubcommentItem');
  const subcomment = subcommentOverride;

  // const handleDeleteSubcomment = useCallback(
  //   () => social.service('comment').remove(subcommentId),
  //   [subcommentId],
  // );

  const [profileAvatar] = useObservable(
    () => social.service('profile-avatar').watch().get(`profileId:${subcomment?.author?.id}`),
    [subcomment?.author?.id],
  );

  const [isTruncated, toggleTruncated] = useToggle(false);
  const [isExpanded, toggleExpanded] = useToggle(false);

  if (!subcomment) return <Pending />;

  return (
    <>
      <Row className="">
        <Col xs="auto" className="d-flex flex-column align-items-center">
          <img
            width="24"
            height="24"
            alt={subcomment.author?.name || 'author'}
            src={profileAvatar?.exists
              ? profileAvatar?.downloadUrl
              : avatar}
            className="rounded-circle"
          />
          {disableLine && (
            <div style={{ background: '#BDBDBD', flexGrow: 1, width: '1px' }} />
          )}
        </Col>
        <Col>
          <Row>
            <Col>
              <small className="font-weight-bold">
                {subcomment.author?.username}
                {' '}
                {subcomment.author?.stripeInfo?.detailsSubmitted
                && subcomment.author?.stripeInfo?.chargesEnabled
                && subcomment.author?.stripeInfo?.payoutsEnabled
                && (
                  <VerifiedSvg style={{ marginBottom: '4px' }} />
                )}
              </small>
            </Col>
            <Col xs="auto">
              <small className="text-black-50">
                <Moment fromNow ago date={moment(subcomment.createdAt)} />
                {' '}
                ago
                {' '}
                {/* <Dropdown as="span">
                  <Dropdown.Toggle variant="link"
                  size="sm" className="text-black-50 p-0" bsPrefix="btn">
                    <FontAwesomeIcon icon={faEllipsisH} />
                  </Dropdown.Toggle>
                  <Dropdown.Menu>
                    <Dropdown.Item
                     onClick={handleDeleteSubcomment}>{t('deleteComment')}</Dropdown.Item>
                  </Dropdown.Menu>
                </Dropdown> */}
              </small>
            </Col>
            <Col xs={11}>
              <p>
                <Truncate
                  lines={!isExpanded && 3}
                  ellipsis={(
                    <span>
                      ...
                      {' '}
                      <a className="text-underline" href="#more" onClick={toggleExpanded}>{t('more')}</a>
                    </span>
                  )}
                  onTruncate={toggleTruncated}
                >
                  {subcomment.message}
                </Truncate>
                {!isTruncated && isExpanded && (
                  <span>
                    {' '}
                    <a href="#less" onClick={toggleExpanded}><u>{t('less')}</u></a>
                  </span>
                )}
              </p>
            </Col>
          </Row>
        </Col>
      </Row>
    </>
  );
};

export default SubcommentItem;
