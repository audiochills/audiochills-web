import React from 'react';
import { useTranslation } from 'react-i18next';
import {
  Col, Row, Form, Modal,
} from 'react-bootstrap';
import { useModalManager } from '@vlzh/react-modal-manager';

export type AddPaymentModalProps = {
};

export const AddPaymentModal: React.FC<AddPaymentModalProps> = () => {
  const { t } = useTranslation('AddPaymentModal');
  const { isOpen, closeModal } = useModalManager('AddPaymentModal');

  return (
    <>
      <Modal centered show={isOpen()} onHide={() => closeModal()}>
        <Modal.Header closeButton>
          <Modal.Title>{t('heading')}</Modal.Title>
        </Modal.Header>
        <Modal.Body className="pt-0">
          <p>{t('paragraph')}</p>
          <Form>
            <Form.Group>
              <Form.Label>{t('nameOnCardLabel')}</Form.Label>
              <Form.Control type="text" placeholder={t('nameOnCardPlaceholder')} />
            </Form.Group>
            <Form.Group>
              <Form.Label>{t('numberOnCardLabel')}</Form.Label>
              <Form.Control type="number" placeholder={t('numberOnCardPlaceholder')} />
            </Form.Group>
            <Row>
              <Col>
                <Form.Group>
                  <Form.Label>{t('expOnCardLabel')}</Form.Label>
                  <Form.Control type="text" placeholder={t('expOnCardPlaceholder')} />
                </Form.Group>
              </Col>
              <Col>
                <Form.Group>
                  <Form.Label>{t('cvcOnCardLabel')}</Form.Label>
                  <Form.Control type="text" placeholder={t('cvcOnCardPlaceholder')} />
                </Form.Group>
              </Col>
            </Row>
            <Form.Group>
              <Form.Check type="checkbox" label={t('confirm')} />
            </Form.Group>
          </Form>
        </Modal.Body>
      </Modal>
    </>
  );
};
export default AddPaymentModal;
