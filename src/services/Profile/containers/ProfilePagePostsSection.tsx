/* eslint-disable react/destructuring-assignment */
import React, { useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { ProfileAttributes, PostAttributes, PlanAttributes } from '@audiochills/api-social/src/attributes';
import { Button, Container } from 'react-bootstrap';
import { useModalManager } from '@vlzh/react-modal-manager';
import { useObservable } from 'react-use-observable';
import { of } from 'rxjs';
import { Paginated } from '@feathersjs/feathers';
import moment from 'moment';
import { Pending, social } from '../../../apis/social';
import PostItem from './PostItem';
import keycloak from '../../../keycloak';

export type ProfilePagePostsSectionProps = {
  profileId: string
  profileOverride?: ProfileAttributes
  postsOverride?: Paginated<PostAttributes>
  planOverride?: PlanAttributes
};

export const ProfilePagePostsSection:
React.FC<ProfilePagePostsSectionProps> = ({
  profileId, profileOverride, postsOverride, planOverride,
}) => {
  const { t } = useTranslation('ProfilePagePostsSection');
  const { openModal } = useModalManager();

  const [authentication] = useObservable(() => social.authentication.result(), []);

  const [profile, profileError] = useObservable(
    () => (typeof profileOverride !== 'undefined' ? of(profileOverride) : social.service('profile').watch().get(`${profileId}`)), [profileId, profileOverride],
  );
  const [posts, postsError] = useObservable(
    () => (typeof postsOverride !== 'undefined' ? of(postsOverride) : social.service('post').watch().find({ query: { authorId: profileId, isPublished: true } })), [profileId, postsOverride],
  );
  const [plan, planError] = useObservable(
    () => (typeof planOverride !== 'undefined' ? of(planOverride) : social.service('plan').watch().get(`profileId:${profileId}`)), [profileId, planOverride],
  );
  const [subscriptions, subscriptionsError] = useObservable(
    () => (!authentication ? of(null) : social.service('subscription').watch().find({
      query: {
        $sort: {
          createdAt: -1,
        },
        creatorId: profileId,
        fanId: authentication.profile.id,
        // renewalDate: { $gt: new Date() },
      },
      // where: { creatorId: profileId, fanId: authentication?.profile?.id },
    })), [authentication],
  );

  const subscription = useMemo(
    () => subscriptions && subscriptions.data,
    [subscriptions],
  );

  const canSubscribe = (): true | string => {
    const subscriptionDB = subscription as any;

    if (subscriptionDB && subscriptionDB.length > 0) {
      if (subscriptionDB[0].isCancelled === null
        || subscriptionDB[0].renewalDate === null) {
        // console.log('subscription doesn"t exist');
        return true;
      }

      // subscription isn't cancelled, it's still
      // running and renewal succeeds (renewal date > current date)
      if (subscriptionDB[0].isCancelled === false
        && moment(subscriptionDB[0].renewalDate) > moment()) {
      // console.log('Subscription active');
        return `Subscription active for $ ${(plan?.amountCents ?? 0) / 100}`;
      }

      // subscription is cancelled but the due data isn't finished
      if (subscriptionDB[0].isCancelled
        && moment(subscriptionDB[0].renewalDate) > moment()) {
      // console.log('Subscription is cancelled but the due data isn\'t finished');
        return `Subscription active, will renew on due date for $${(plan?.amountCents ?? 0) / 100}`;
      }

      if (authentication && (profile?.id === authentication?.profile?.id)) {
      // can't subscribe to yourself
        return true;
      }

      // subscription is cancelled and expired
      // console.log('subscription is cancelled and expired');
      return true;
    }
    // console.log('subscription doesn"t exist');
    return true;
  };

  if (profileError) throw profileError;
  if (postsError) throw postsError;
  if (planError) throw planError;
  if (subscriptionsError) throw subscriptionsError;
  return (
    <>
      {profile === null && <Pending />}
      {posts === null && <Pending />}
      {plan === null && <Pending />}
      {authentication === null && <Pending />}
      {authentication && subscription === null && <Pending />}
      <Container>
        {
          (plan?.priceId && plan?.productId
           && profile?.id !== (authentication as any)?.profile?.id)

          && (
            <>
              <p className="mt-3">{t('subscriptionMessage')}</p>
              <p>
                {
                  typeof canSubscribe() === 'string'
                    ? (
                      <>
                        {canSubscribe()}
                      </>
                    )
                    : (
                      <Button
                        variant="primary"
                        disabled={typeof canSubscribe() !== 'boolean'}
                        onClick={async () => {
                          if (!authentication) keycloak.login();
                          openModal('ProfileSubscribeModal', false, { profileId: profile?.id, profileOverride: profile });
                        }}
                      >
                        {/* {t('subscribeButton', { profile })} */}

                        Subscribe for $
                        {plan?.amountCents / 100}

                      </Button>
                    )
                }

              </p>
            </>
          )

        }

      </Container>
      {posts && posts.data.map((post) => (
        <PostItem postId={post.id} postOverride={post} key={post.id} />
      ))}

      <p style={{ marginTop: '200px' }} />
    </>
  );
};
export default ProfilePagePostsSection;
