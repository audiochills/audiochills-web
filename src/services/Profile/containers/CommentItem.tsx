import React, { useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import {
  Col, Row, Form, Button, Collapse,
} from 'react-bootstrap';

import { CommentAttributes } from '@audiochills/api-social/src/attributes';
import Moment from 'react-moment';
import { useObservable, useRetryableObservable } from 'react-use-observable';
import { useAsyncFn, usePreviousDistinct, useToggle } from 'react-use';
import Truncate from 'react-truncate';
import moment from 'moment';
import { social } from '../../../apis/social';
import avatar from '../images/avatar-placeholder.png';
import { ReactComponent as VerifiedSvg } from '../images/verified.svg';
import SubcommentItem from './SubcommentItem';
import keycloak from '../../../keycloak';

export type CommentItemProps = {
  commentId: string
  commentOverride?: CommentAttributes
  disableLine?: boolean
};

export const CommentItem:
React.FC<CommentItemProps> = ({ commentId, commentOverride, disableLine = false }) => {
  const { t } = useTranslation('CommentItem');
  const comment = commentOverride;
  const [currentSubcomments, , , retrySubcomments] = useRetryableObservable(
    () => social.service('comment').watch().find({
      query: {
        $sort: {
          createdAt: -1,
        },
        parentId: commentId,
      },
    }), [commentId],
  );
  const prevSubcomments = usePreviousDistinct(currentSubcomments, (prev, next) => !!prev && !next);
  const subcomments = useMemo(
    () => currentSubcomments || prevSubcomments,
    [currentSubcomments, prevSubcomments],
  );

  // const handleDeleteComment = useCallback(
  //   () => social.service('comment').remove(commentId),
  //   [commentId],
  // );
  const [isReplyDisplayed, toggleReplyDisplayed] = useToggle(false);

  const [authentication] = useObservable(() => social.authentication.result(), []);

  const [createCommentSubmitState, createCommentSubmit] = useAsyncFn(
    async (e) => {
      e.preventDefault();
      if (!authentication) return keycloak && await keycloak.login();
      await social.service('comment').create({ authorId: authentication.profile.id, parentId: commentId, ...Object.fromEntries(new FormData(e.target)) });
      e.target.reset();
      retrySubcomments();
      return undefined;
    },
    [commentId, authentication],
  );

  const [profileAvatar] = useObservable(
    () => social.service('profile-avatar').watch().get(`profileId:${comment?.author?.id}`),
    [comment?.author?.id],
  );

  const [isTruncated, toggleTruncated] = useToggle(false);
  const [isExpanded, toggleExpanded] = useToggle(false);

  if (!comment) return <></>;
  if (!subcomments) return <></>;

  return (
    <>
      <Row className="">
        <Col xs="auto" className="d-flex flex-column align-items-center">
          <img
            width="24"
            height="24"
            alt={comment.author?.name || 'author'}
            src={profileAvatar?.exists
              ? profileAvatar?.downloadUrl
              : avatar}
            className="rounded-circle"
          />
          {disableLine && (
            <div style={{ background: '#BDBDBD', flexGrow: 1, width: '1px' }} />
          )}
        </Col>
        <Col>
          <Row>
            <Col>
              <small className="font-weight-bold">
                {comment.author?.username || 'author'}
                {' '}
                {comment.author?.stripeInfo?.detailsSubmitted
                && comment.author?.stripeInfo?.chargesEnabled
                && comment.author?.stripeInfo?.payoutsEnabled
                && (
                  <VerifiedSvg style={{ marginBottom: '4px' }} />
                )}
              </small>
            </Col>
            <Col xs="auto">
              <small className="text-black-50">
                <Moment fromNow ago date={moment(comment?.createdAt)} />
                {' '}
                ago
                {' '}
                {/* <Dropdown as="span">
                  <Dropdown.Toggle variant="link"
                   size="sm" className="text-black-50 p-0" bsPrefix="btn">
                    <FontAwesomeIcon icon={faEllipsisH} />
                  </Dropdown.Toggle>
                  <Dropdown.Menu>
                    <Dropdown.Item
                    onClick={handleDeleteComment}>{t('deleteComment')}</Dropdown.Item>
                  </Dropdown.Menu>
                </Dropdown> */}
              </small>
            </Col>
          </Row>

          <p className="mb-0">
            <Truncate
              lines={!isExpanded && 3}
              ellipsis={(
                <span>
                  ...
                  {' '}
                  <a className="text-underline" href="#more" onClick={toggleExpanded}>{t('more')}</a>
                </span>
                  )}
              onTruncate={toggleTruncated}
            >
              {comment.message}
            </Truncate>
            {!isTruncated && isExpanded && (
            <span>
              {' '}
              <a href="#less" onClick={toggleExpanded}><u>{t('less')}</u></a>
            </span>
            )}
          </p>

          <>
            <p><small><a href="#reply" className="text-muted" onClick={toggleReplyDisplayed}>{t('replyComment')}</a></small></p>
            <Collapse in={isReplyDisplayed}>
              <Form onSubmit={createCommentSubmit}>
                <Row className="no-gutters border pt-3 px-3 rounded mb-3">
                  <Col>
                    <p>
                      <Form.Control
                        size="sm"
                        type="text"
                        placeholder={t('commentMessagePlaceholder')}
                        required
                        name="message"
                      />
                    </p>
                  </Col>
                  <Col xs="auto" className="ml-2">
                    <p>
                      <Button size="sm" variant="primary" type="submit" disabled={createCommentSubmitState.loading}>
                        {t('submitComment')}
                      </Button>
                    </p>
                  </Col>
                </Row>
              </Form>
            </Collapse>
          </>

          {subcomments.data.map((subcomment, idx, data) => (
            <SubcommentItem
              key={subcomment.id}
              subcommentOverride={subcomment}
              subcommentId={subcomment.id}
              disableLine={idx + 1 < data.length}
            />
          ))}
        </Col>
      </Row>
    </>
  );
};

export default CommentItem;
