import React, { useState, useCallback } from 'react';
import { useTranslation } from 'react-i18next';
import {
  Button, Card, Col, Row, Modal,
} from 'react-bootstrap';
import { PostAttributes } from '@audiochills/api-social/src/attributes';
import { useModalManager } from '@vlzh/react-modal-manager';
import { of } from 'rxjs';
import { useObservable } from 'react-use-observable';
import CurrencyInput from 'react-currency-input-field';
import { useAsyncFn } from 'react-use';
import { social } from '../../../apis/social';
import AcLogo from '../images/acLogo.svg';

export type TipPostModalProps = {
};

export type TipPostModalParams = null | {
  postId?: string
  postOverride?: PostAttributes
};

export const TipPostModal: React.FC<TipPostModalProps> = () => {
  const { t } = useTranslation('TipPostModal');
  const {
    isOpen, closeModal, getParams,
  } = useModalManager('TipPostModal');
  const params = getParams() as TipPostModalParams;
  const { postId, postOverride } = params || { postOverride: null };
  const [post] = useObservable(
    () => (typeof postOverride !== 'undefined' ? of(postOverride) : social.service('post').watch().get(`${postId}`)),
    [postId, postOverride],
  );
  const [postImage] = useObservable(
    () => (!postId ? of(null) : social.service('post-image').watch().get(`postId:${postId}`)),
    [postId, postOverride],
  );

  const [profileAvatar] = useObservable(
    () => (!post ? of(null) : social.service('profile-avatar').watch().get(`profileId:${post?.authorId}`)),
    [post?.authorId, postOverride],
  );

  const [tipValueCents, setTipValueCents] = useState<number>(5 * 100);

  const registerTip = useCallback((val) => {
    setTipValueCents(parseInt(val, 10) * 100);
  }, [tipValueCents]);

  const [, doStripeCheckout] = useAsyncFn(async () => {
    const stripeUrl = await (social.service('tip-post') as any).patch(null, { amountCents: tipValueCents, postId: post?.id ?? '' });
    window.location.href = stripeUrl.url;
  }, [tipValueCents, post]);

  return (
    <>
      <Modal centered show={isOpen() && post} onHide={() => closeModal()} style={{ zIndex: 9000 }}>
        {post && (
        <>
          <Modal.Header
            closeButton
            className="overflow-hidden"
            style={(postImage as any)?.exists
              ? { background: `no-repeat center/100% url(${(postImage as any)?.downloadUrl})`, height: '120px' }
              : { background: `no-repeat center/100% url(${AcLogo})`, height: '120px' }}
          />
          <Modal.Body className="pt-0" style={{ backgroundColor: '#FFF2E1' }}>
            <Row className="no-gutters">
              <Col xs="auto">
                <Card
                  style={{
                    border: '2px solid rgb(255,255,255)',
                    height: '120px',
                    marginLeft: '-2px',
                    marginRight: '-2px',
                    marginTop: '-60px',
                    width: '120px',
                  }}
                  className="overflow-hidden"
                >
                  <Card.Img
                    src={(profileAvatar as any)?.exists
                      ? profileAvatar?.downloadUrl
                      : AcLogo}
                    width={120}
                    height={120}
                  />
                </Card>
              </Col>
              <Col className="m-2">
                <p className="font-weight-bold mb-0">
                  {post?.author?.name}
                </p>
                <p className="text-muted mb-0">
                  {`@${post?.author?.username}`}
                </p>
              </Col>
            </Row>

            <p className="font-weight-bold mt-3">
              Tip
            </p>
            <CurrencyInput
              maxLength={6}
              className="mt-1 mb-2 text-center w-100"
              id="input-example"
              name="input-name"
              placeholder="5.99"
              prefix="$"
              style={{
                borderColor: 'grey', borderRadius: '4px', borderStyle: 'solid', borderWidth: '0.3px',
              }}
              defaultValue={5}
              decimalsLimit={2}
              onValueChange={registerTip}
            />

            <p className="text-center mt-3">
              <Button
                variant="primary"
                style={{ paddingBottom: '13px', paddingTop: '13px' }}
                block
                onClick={() => doStripeCheckout()}
              >
                {t('tip')}

              </Button>
            </p>
          </Modal.Body>
        </>
        )}
      </Modal>
    </>
  );
};

export default TipPostModal;
