import React, {
  useCallback, useMemo,
} from 'react';
import { useTranslation } from 'react-i18next';
import {
  Button, Card, Col, Row, Container,
} from 'react-bootstrap';
import { PostAttributes } from '@audiochills/api-social/src/attributes';
import Moment from 'react-moment';
import { Link, generatePath } from 'react-router-dom';
import { useModalManager } from '@vlzh/react-modal-manager';
import { useObservable, useRetryableObservable } from 'react-use-observable';
import { of } from 'rxjs';
import { LinkContainer } from 'react-router-bootstrap';
import { usePreviousDistinct } from 'react-use';
import moment from 'moment';
import { Pending, social } from '../../../apis/social';
import BgPlaceHolder from '../images/bg-placeholder.png';
import LockSvg from '../images/lock.svg';
import { ReactComponent as VerifiedSvg } from '../images/verified.svg';
import { ReactComponent as TipIcon } from '../images/reactionTip.svg';
import { ReactComponent as CommentIcon } from '../images/reactionComment.svg';
import { ReactComponent as ShareIcon } from '../images/reactionShare.svg';
import { ReactComponent as HeartIcon } from '../images/reactionHeart.svg';

// import HeartSvgActive from '../images/reactionHeartRed.svg';

import WebShare from './WebShare';
import { eventNames, logEvent } from '../../../analytics';
import keycloak from '../../../keycloak';

export type PostItemProps = {
  postId: string
  postOverride?: PostAttributes
};

const ReactionsGrey = { color: 'darkgray ' };

export const PostItem: React.FC<PostItemProps> = ({ postId, postOverride }) => {
  const { t } = useTranslation('PostItem');

  const [authentication] = useObservable(() => social.authentication.result(), []);

  const [curPost, , , retryPost] = useRetryableObservable(
    () => (typeof postOverride !== 'undefined' ? of(postOverride) : social.service('post').watch().get(`${postId}`)),
    [postId, postOverride],
  );
  const prevPost = usePreviousDistinct(curPost, (prev, next) => !next);
  const post = useMemo(() => curPost || prevPost, [curPost, prevPost]);

  const [postImage] = useObservable(
    () => social.service('post-image').watch().get(`postId:${post?.id}`),
    [post?.id],
  );
  const [postAudio] = useObservable(
    () => (social.service('post-audio').watch().get(`postId:${postId}`)),
    [postId],
  );

  const [profileLikePost] = useObservable(
    () => (!authentication
      ? of(null)
      : ((social.service('profile-like-post') as any).watch().find({ query: { postId, profileId: authentication.profile.id } }))),
    [post, postId, authentication],
  );

  const [stripeInfo] = useObservable(
    () => social.service('stripe-info').watch().get(`profileId:${post?.authorId}`),
    [post?.authorId],
  );

  const descriptionIsTooLong = React.useMemo(
    () => post && post.description.length > 100,
    [post],
  );

  // const [, handleDeletePost] = useAsyncFn(
  //   () => social.service('post').remove(postId),
  //   [postId],
  // );
  const [descriptionIsTruncated, setDescriptionIsTruncated] = React.useState(true);
  const toggleDescriptionIsTruncated = React.useCallback(
    () => setDescriptionIsTruncated(!descriptionIsTruncated),
    [descriptionIsTruncated],
  );
  const truncatedDescription = React.useMemo(
    () => post?.description.slice(0, 100),
    [post],
  );
  const { openModal } = useModalManager();

  const manageLike = useCallback(async () => {
    if (!authentication) return keycloak && await keycloak.login();
    if (!(profileLikePost as any)?.data?.length) {
      await (social.service('profile-like-post') as any).create({ postId: post!.id });
    } else {
      await (social.service('profile-like-post') as any).remove((profileLikePost as any)?.data[0]?.id);
    }
    retryPost();
    return undefined;
  }, [post, profileLikePost, (profileLikePost as any)?.id]);

  const localLogEvent = async () => {
    const creator = await social.service('profile').get(`${post?.author?.id}`);
    logEvent(eventNames.PLAY_CREATION, {
      amountTips: ((post as any)?.tipTotalCents ?? 0) / 100,
      category: post?.genre?.category?.id,
      comments: '',
      creatorUsername: post?.author?.username,
      following: authentication ? authentication?.profile?.followerCount : -1,
      genre: post?.genreId,
      likes: post?.likeCount,
      monetisation: post?.isPaid ? 'Paid' : 'Free',
      price: (post?.price || 0) / 100,
      tags: post?.customTags,
      userAudience: creator.followerCount,
    });
  };
  const playAudio = useCallback(() => {
    openModal('BottomPlayerModal', true, { post });
    localLogEvent();
  }, [post]);

  const playOrPay = useCallback(async () => {
    if ((postAudio as any)?.isAuthorized) {
      // eslint-disable-next-line no-console
      console.log('play');
      playAudio();
    } else {
      if (!authentication) return keycloak && await keycloak.login();
      openModal('UnlockPostModal', false, { postId: post!.id, postOverride: post });
    }
    return undefined;
  }, [post, postAudio]);

  const openTipModal = useCallback(() => {
    if (!authentication) return keycloak && keycloak.login();
    openModal('TipPostModal', false, { postId: post?.id, postOverride: post });
    return undefined;
  }, [post]);

  return (
    <>
      {post === null && <Pending />}
      <Container className="pt-2 pb-2">
        <Row className="mt-2">
          <Col xs="auto">
            <small className="font-weight-bold align-text-bottom">
              {post?.author?.username && (
                <Link to={generatePath('/profile/:profileUsername', { profileUsername: post.author?.username })} className="text-dark">{ post.author?.username }</Link>
              )}
              {' '}
              {stripeInfo?.detailsSubmitted && stripeInfo?.chargesEnabled
              && stripeInfo?.payoutsEnabled && (<VerifiedSvg style={{ marginBottom: '4px' }} />) }
              {' '}
              {/* {post.author?.isNsfw && (
                <Badge variant="secondary">{t('nsfwBadge')}</Badge>
              )} */}
            </small>
          </Col>
          <Col xs="auto" className="ml-auto">
            <small className="text-black-50">
              <Moment fromNow ago date={moment(post?.createdAt)} />
              {' '}
              ago
              {' '}
              {/* <Dropdown as="span">
                <Dropdown.Toggle variant="link"
                size="sm" className="text-black-50 p-0" bsPrefix="btn">
                  <FontAwesomeIcon icon={faEllipsisH} />
                </Dropdown.Toggle>
                <Dropdown.Menu>
                  <Dropdown.Item onClick={handleDeletePost}>{t('deletePost')}</Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown> */}
            </small>
          </Col>
        </Row>
        <p className={`mt-2 ${!descriptionIsTooLong ? 'mb-2' : 'mb-0'}`}>
          {descriptionIsTooLong && descriptionIsTruncated
            ? truncatedDescription : post?.description}
        </p>
        {descriptionIsTooLong && (
          <p className="mt-0">
            <small>
              <a className="text-muted" href={descriptionIsTruncated ? '#more' : '#less'} onClick={toggleDescriptionIsTruncated}>
                {descriptionIsTruncated && (<>Show more</>)}
                {!descriptionIsTruncated && (<>Show less</>)}
              </a>
            </small>
          </p>
        )}
        <Row className="mb-2">
          <Col xs="auto">
            <Button
              onClick={playOrPay}
              className="p-0"
              style={{
                backgroundColor: 'transparent', border: 'none', height: '100px', width: '104px',
              }}
            >

              <Card>
                <Card.Img
                  variant="top"
                  style={{
                    borderRadius: '15px', height: '104px', objectFit: 'cover', overflow: 'hidden',
                  }}
                  src={(postImage as any)?.exists
                    ? (postImage as any)?.downloadUrl
                    : BgPlaceHolder}
                  title={t('profileimage', { post })}
                />
                {
                  !(postAudio as any)?.isAuthorized
                  && (
                    <Card.Img
                      className="p-4"
                      style={{
                        backgroundColor: 'rgba(255, 255, 255, 0.6)',
                        height: '100%',
                        left: '0',
                        position: 'absolute',
                        top: '0',
                        width: '100%',
                      }}
                      src={LockSvg}
                    />
                  )
                }

              </Card>
            </Button>

          </Col>
          <Col className="pl-0">
            <p className="text-black-50 mb-0">
              <small>
                {post?.genre?.category?.id && (
                <span className="text-black-50">{post?.genre?.category?.name?.replace('_', ' ')}</span>
                )}
                {' '}
                /
                {' '}
                <span className="text-black-50">{ post?.genre?.name?.replace('_', ' ') }</span>
              </small>
            </p>
            <p className="mt-1 mb-0"><Link to={generatePath('/post/:postId', { postId: post?.id || false })} className="font-weight-bold text-dark">{post?.title}</Link></p>
          </Col>
        </Row>
        <p className="mb-2 mt-3" style={ReactionsGrey}>

          <Button
            variant="link"
            size="sm"
            className="mr-1"
            onClick={manageLike}
            style={{ color: ((profileLikePost as any)?.data?.length) ? 'red' : 'darkgray ' }}
          >
            <HeartIcon />
            {' '}
            {post?.likeCount}
          </Button>

          <LinkContainer to={`/post/${post?.id}`} exact>
            <Button variant="link" size="sm" className="text-black-50">
              <CommentIcon />
              {' '}
              {post?.commentCount}
            </Button>
          </LinkContainer>

          {stripeInfo?.detailsSubmitted && stripeInfo?.chargesEnabled
              && stripeInfo?.payoutsEnabled && (
              <Button
                variant="link"
                size="sm"
                className="mx-1"
                style={ReactionsGrey}
                onClick={openTipModal}
              >
                {/* <FontAwesomeIcon icon={faCoins} /> */}
                <TipIcon />
                {' '}
                <small>$</small>
                {((post as any)?.tipTotalCents ?? 0) / 100}
              </Button>
          ) }

          <WebShare
            title="Audiochills"
            text=""
            url={`https://audiochills.art/post/${post?.id}`}
          >
            <Button
              className="mx-1"
              variant="link"
              size="sm"
              style={ReactionsGrey}
            >
              <ShareIcon />
            </Button>
          </WebShare>

        </p>
      </Container>
      <hr className="m-0" />
    </>
  );
};

export default PostItem;
