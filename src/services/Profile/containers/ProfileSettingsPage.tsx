import React, {
  useCallback,
} from 'react';
import { Trans, useTranslation } from 'react-i18next';
import {
  Button, Col, Container, Form, Row, Alert, Spinner,
} from 'react-bootstrap';
import { useObservable, useRetryableObservable } from 'react-use-observable';
import { useAsyncFn, useDebounce } from 'react-use';
import { of } from 'rxjs';
import { Formik, useFormikContext } from 'formik';
import * as yup from 'yup';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import Dropzone from 'react-dropzone';
import { useModalManager } from '@vlzh/react-modal-manager';
import { ProfileAttributes } from '@audiochills/api-social/src/attributes';
import { Pending, social } from '../../../apis/social';
import Avatar from '../components/Avatar';
import keycloak from '../../../keycloak';

export type ProfileSettingsProps = {
};

const schema = yup.object().shape({
  aboutMessage: yup.string().required(),
  name: yup.string().required(),
  username: yup.string()
    .matches(/^(?=.{2,30}$)(?![_.])(?!.*[_.]{2})[a-zA-Z0-9._]+(?<![_.])$/,
      'username contains special characters, short, or too long').required(),
});

// https://codereview.stackexchange.com/questions/135363/filtering-undefined-elements-out-of-an-array
export function filterUndef<T>(ts: (T | undefined)[]): T[] {
  return ts.filter((t: T | undefined): t is T => !!t);
}

const FormikAutoSave: React.FC<{ debounceMs: number }> = ({ debounceMs }) => {
  const ctx = useFormikContext();
  useDebounce(
    () => ctx.isValid && ctx.dirty && ctx.submitForm(),
    debounceMs,
    [ctx.submitForm, ctx.isValid, ctx.initialValues, ctx.values, debounceMs],
  );
  return null;
};

export const ProfileSettings: React.FC<ProfileSettingsProps> = () => {
  const { t } = useTranslation('ProfileSettings');
  const { openModal } = useModalManager();

  const [authentication] = useObservable(() => social.authentication.result(), []);

  const [profileAvatar, profileAvatarError, , retryProfileAvatar] = useRetryableObservable(
    () => (!authentication ? of(null) : social.service('profile-avatar').watch().get(`profileId:${authentication.profile.id}`)),
    [authentication && authentication.profile.id],
  );
  const [profileBanner, profileBannerError, , retryProfileBanner] = useRetryableObservable(
    () => (!authentication ? of(null) : social.service('profile-banner').watch().get(`profileId:${authentication.profile.id}`)),
    [authentication && authentication.profile.id],
  );
  const [categories, categoriesError] = useObservable(
    () => social.service('category').watch().find(),
    [],
  );
  const [profileHasCategories, profileHasCategoriesError] = useObservable(
    () => (!authentication ? of(null) : social.service('profile-has-category').watch().find({ query: { profileId: authentication.profile.id } })),
    [authentication && authentication.profile.id],
  );
  const profileCategories = React.useMemo(
    () => profileHasCategories
          && filterUndef(profileHasCategories.data.map(
            (profileHasCategory) => profileHasCategory.category
                                    && ({ ...profileHasCategory.category, profileHasCategory }),
          )),
    [profileHasCategories],
  );

  const selectableCategories = React.useMemo(
    () => categories && profileCategories
          && categories.data.filter(
            (category) => profileCategories.every(({ id }) => id !== category.id),
          ),
    [categories, profileCategories],
  );

  const [, handleSubmitProfile] = useAsyncFn(
    async (data: Partial<ProfileAttributes>) => {
      if (!authentication) return;
      if (!data) return;
      await social.service('profile').patch(authentication.profile.id, data);
    },
    [authentication && authentication.profile.id],
  );

  const [addCategoryState, addCategory] = useAsyncFn(
    async (categoryId: string) => {
      if (!authentication) return;
      if (!categoryId) return;
      await social.service('profile-has-category').create({ categoryId, profileId: authentication.profile.id });
    },
    [authentication && authentication.profile.id],
  );
  const [removeCategoryState, removeCategory] = useAsyncFn(
    async (profileHasCategoryId: string) => {
      if (!authentication) return;
      await social.service('profile-has-category').remove(profileHasCategoryId);
    },
    [authentication && authentication.profile.id],
  );
  // console.log(authentication);
  const [welcomeAudioState, onDropWelcomeAudioDropzone] = useAsyncFn(
    async (acceptedFiles: File[]) => {
      if (!authentication) return;
      const [body] = acceptedFiles;
      if (!authentication.profile) throw new Error('Wrong server config.');
      const pa = await social.service('profile-audio').patch(authentication.profile?.profileAudio?.id ?? '_', { mimeType: body.type });
      if (Array.isArray(pa) || !pa.uploadUrl) throw new Error('Wrong server config.');
      await fetch(pa.uploadUrl, { body, method: 'PUT' });
    },
    [authentication && authentication.profile.id],
  );
  const onDropProfileAvatarDropzone = useCallback(
    (acceptedFiles: File[]) => {
      const [file] = acceptedFiles;
      openModal('ProfileSettingsImageModal', false, {
        onDone: async (canvas: HTMLCanvasElement) => {
          if (!authentication) return;
          const body = await new Promise<Blob | null>((resolve) => canvas.toBlob(resolve));
          const pa = await social.service('profile-avatar').patch(authentication.profile?.profileAvatar?.id ?? '_', { mimeType: 'image/png' });
          if (Array.isArray(pa) || !pa.uploadUrl) throw new Error('Wrong server config.');
          await fetch(pa.uploadUrl, { body, method: 'PUT' });
          retryProfileAvatar();
        },
        src: URL.createObjectURL(file),
      });
    },
    [authentication && authentication.profile.id],
  );

  const onDropProfileBannerDropzone = useCallback(
    (acceptedFiles: File[]) => {
      const [file] = acceptedFiles;
      openModal('ProfileSettingsImageModal', false, {
        height: 160,
        onDone: async (canvas: HTMLCanvasElement) => {
          if (!authentication) return;
          const body = await new Promise<Blob | null>((resolve) => canvas.toBlob(resolve));
          const pa = await social.service('profile-banner').patch(authentication.profile?.profileBanner?.id ?? '_', { mimeType: 'image/png' });
          if (Array.isArray(pa) || !pa.uploadUrl) throw new Error('Wrong server config.');
          await fetch(pa.uploadUrl, { body, method: 'PUT' });
          retryProfileBanner();
        },
        src: URL.createObjectURL(file),
        width: 800,
      });
    },
    [authentication && authentication.profile.id],
  );

  // if (authentication === false) keycloak.login();
  if (categoriesError) throw categoriesError;
  if (profileAvatarError) throw profileAvatarError;
  if (profileBannerError) throw profileBannerError;
  if (profileHasCategoriesError) throw profileHasCategoriesError;

  if (authentication === false) keycloak.login();

  return (
    <>
      {categories === null && <Pending />}
      {profileAvatar === null && <Pending />}
      {profileBanner === null && <Pending />}
      {profileHasCategories === null && <Pending />}
      {authentication && authentication.profile && (
      <Container className="mt-3">
        <Trans i18nKey="introduction" t={t}>
          <h1>heading</h1>
          <p>paragraph</p>
        </Trans>
        <Formik
          validationSchema={schema}
          initialValues={authentication.profile}
          onSubmit={handleSubmitProfile}
        >
          {({
            handleSubmit,
            handleChange,
            values,
            touched,
            errors,
          }) => (
            <Form noValidate onSubmit={handleSubmit}>
              <label htmlFor="profileAvatar">
                <small><strong>{t('profileAvatar')}</strong></small>
                {' '}
              </label>
              <Dropzone onDrop={onDropProfileAvatarDropzone} accept="image/*">
                {({ getRootProps, getInputProps, open }) => (
                  <p>
                    {/* eslint-disable-next-line react/jsx-props-no-spreading */}
                    <div {...getRootProps()}><input {...getInputProps()} /></div>
                    <Button
                      variant="link"
                      className="p-0"
                      onClick={open}
                      style={{ maxWidth: '100%' }}
                    >
                      <Avatar src={(profileAvatar as any)?.exists ? profileAvatar?.downloadUrl : 'https://dummyimage.com/160x160/fff/aaa'} className="shadow" />
                    </Button>
                  </p>
                )}
              </Dropzone>
              <label htmlFor="profileBanner"><small><strong>{t('profileBanner')}</strong></small></label>
              <Dropzone onDrop={onDropProfileBannerDropzone} accept="image/*">
                {({ getRootProps, getInputProps, open }) => (
                  <p>
                    {/* eslint-disable-next-line react/jsx-props-no-spreading */}
                    <div {...getRootProps()}><input {...getInputProps()} /></div>
                    <Button
                      variant="link"
                      className="p-0"
                      onClick={open}
                      style={{ maxWidth: '100%' }}
                    >
                      <Avatar width="800px" height="160px" src={(profileBanner as any)?.exists ? profileBanner?.downloadUrl : 'https://dummyimage.com/800x160/fff/aaa'} className="shadow" />
                    </Button>
                  </p>
                )}
              </Dropzone>
              <label htmlFor="welcomeAudio"><small><strong>{t('welcomeAudio')}</strong></small></label>
              {welcomeAudioState.error && (
                <p>
                  <Alert variant="danger">{welcomeAudioState.error.toString()}</Alert>
                </p>
              )}
              <Dropzone onDrop={onDropWelcomeAudioDropzone} accept="audio/*">
                {({ getRootProps, getInputProps, open }) => (
                  <p>
                    {/* eslint-disable-next-line react/jsx-props-no-spreading */}
                    <div {...getRootProps()}><input {...getInputProps()} /></div>
                    <Button
                      onClick={open}
                      id="welcomeAudio"
                      disabled={welcomeAudioState.loading}
                    >
                      <small><p className="text-light mb-0">{t('welcomeAudioUpload')}</p></small>
                    </Button>
                    {' '}
                    {welcomeAudioState.loading && (
                      <Spinner className="ml-2" animation="border" style={{ verticalAlign: 'middle' }} />
                    )}
                  </p>
                )}
              </Dropzone>

              <Form.Group controlId="categories">
                <Form.Label>
                  <small><strong>{t('profileCategoriesLabel')}</strong></small>
                </Form.Label>
                <Row noGutters className="m-n1">
                  {profileCategories?.map((category) => category && (
                  <Col xs="auto">
                    <Button disabled={addCategoryState.loading || removeCategoryState.loading} className="m-1" key={category.id} variant="secondary" size="sm" onClick={() => removeCategory(category.profileHasCategory.id)}>
                      {category.name}
                      {' '}
                      <FontAwesomeIcon icon={faTimes} />
                    </Button>
                  </Col>
                  ))}
                  {!!selectableCategories?.length && (
                  <Col xs="auto" className="m-1">
                    <Form.Control disabled={addCategoryState.loading || removeCategoryState.loading} value="" as="select" custom size="sm" onChange={(e) => addCategory(e.target.value)}>
                      <option value="" hidden>{t('profileCategoriesPlaceholder')}</option>
                      {selectableCategories?.map((category) => (
                        <option key={category.id} value={category.id}>{category.name}</option>
                      ))}
                    </Form.Control>
                  </Col>
                  )}
                  {(addCategoryState.loading || removeCategoryState.loading) && (
                  <Col xs="auto" className="m-1">
                    <Spinner animation="border" />
                  </Col>
                  )}
                </Row>
              </Form.Group>
              <Form.Group controlId="username">
                <Form.Label>
                  <small><strong>{t('profileUsernameLabel')}</strong></small>
                  {' '}
                </Form.Label>
                <Form.Control
                  type="text"
                  name="username"
                  value={values.username}
                  isValid={touched.username && !errors.username}
                  isInvalid={!!errors.username}
                  placeholder={t('profileUsernamePlaceholder')}
                  onChange={handleChange}
                  // onChange={(e) => {
                  //   handleChange(e);
                  //   setLocalUsername(e.target.value);
                  // }}
                />
                <Form.Control.Feedback type="invalid">{errors.username}</Form.Control.Feedback>
              </Form.Group>
              <Form.Group controlId="name">
                <Form.Label>
                  <small><strong>{t('profileNameLabel')}</strong></small>
                  {' '}
                </Form.Label>
                <Form.Control
                  type="text"
                  name="name"
                  value={values.name}
                  isValid={touched.name && !errors.name}
                  isInvalid={!!errors.name}
                  placeholder={t('profileNamePlaceholder')}
                  onChange={handleChange}
                  // onChange={(e) => {
                  //   handleChange(e);
                  //   setLocalUsername(e.target.value);
                  // }}
                />
                <Form.Control.Feedback type="invalid">{errors.name}</Form.Control.Feedback>
              </Form.Group>
              <Form.Group controlId="aboutMessage">
                <Form.Label>
                  <small><strong>{t('profileAboutMessageLabel')}</strong></small>
                </Form.Label>
                <Form.Control
                  rows={10}
                  as="textarea"
                  name="aboutMessage"
                  value={values?.aboutMessage ?? ''}
                  isValid={touched.aboutMessage && !errors.aboutMessage}
                  isInvalid={!!errors.aboutMessage}
                  placeholder={t('profileAboutMessagePlaceholder')}
                  onChange={handleChange}
                  // onChange={(e) => {
                  //   handleChange(e);
                  //   setlocalDescription(e.target.value);
                  // }}
                />
                <Form.Control.Feedback type="invalid">{errors.aboutMessage}</Form.Control.Feedback>
              </Form.Group>
              <FormikAutoSave debounceMs={400} />
            </Form>
          )}
        </Formik>
        <p style={{ marginBottom: '200px' }} />
      </Container>
      )}
    </>
  );
};
export default ProfileSettings;
