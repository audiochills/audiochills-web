import React, { useCallback, useState, useRef } from 'react';
import { useTranslation } from 'react-i18next';
import {
  Button, Modal, Alert, Spinner,
} from 'react-bootstrap';
import { ProfileAttributes } from '@audiochills/api-social/src/attributes';
import { useModalManager } from '@vlzh/react-modal-manager';
import Cropper from 'react-cropper';
import 'cropperjs/dist/cropper.css';

export type ProfileSettingsImageModalProps = {
};

export type ProfileSettingsImageModalParams = null | {
  profileId?: string
  profileOverride?: ProfileAttributes
};

export const ProfileSettingsImageModal: React.FC<ProfileSettingsImageModalProps> = () => {
  const { t } = useTranslation('ProfileSettingsImageModal');
  const {
    isOpen, closeModal, getParams,
  } = useModalManager('ProfileSettingsImageModal');

  const [loading, setLoading] = useState<boolean>(false);
  const [error, setError] = useState<Error | null>(null);

  const {
    src, onDone = () => null, width = 160, height = 160,
  } = getParams() || {};
  const cropperRef = useRef<HTMLImageElement>(null);
  const handleUploadButton = useCallback(
    async () => {
      setError(null);
      setLoading(true);
      try {
        const imageElement: any = cropperRef?.current;
        const cropper = imageElement?.cropper;
        await onDone(cropper.getCroppedCanvas({ height, width }));
        closeModal();
      } catch (e) {
        setError(e);
      } finally {
        setLoading(false);
      }
    },
    [closeModal, setError],
  );

  const handleHideModal = useCallback(
    () => { closeModal(); setError(null); },
    [closeModal, setError],
  );

  return (
    <>
      <Modal centered show={isOpen()} onHide={handleHideModal}>
        {isOpen() && (
        <>
          <Modal.Header closeButton>
            <Modal.Title>
              {t('title')}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {error && (
              <Alert variant="danger">
                {error.toString()}
              </Alert>
            )}
            <Cropper
              src={src}
              style={{ height: 400, width: '100%' }}
      // Cropper.js options
              aspectRatio={width / height}
              guides={false}
              ref={cropperRef}
            />
          </Modal.Body>
          <Modal.Footer>
            {loading && (
              <Spinner animation="border" />
            )}
            <Button variant="link" onClick={() => closeModal()} disabled={loading}>{t('cancel')}</Button>
            <Button variant="primary" onClick={handleUploadButton} disabled={loading}>{t('upload')}</Button>
          </Modal.Footer>

        </>
        )}
      </Modal>
    </>
  );
};

export default ProfileSettingsImageModal;
