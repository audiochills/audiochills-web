import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import {
  Button, Container, Row, Col, Image,
} from 'react-bootstrap';
import { ProfileAttributes } from '@audiochills/api-social/src/attributes';
import ReactPlayer from 'react-player';
import { useObservable } from 'react-use-observable';
import { of } from 'rxjs';
import Pause from '../images/pause.svg';
import Play from '../images/play.svg';
import social, { Pending } from '../../../apis/social';

export type ProfilePageAboutSectionProps = {
  profileId: string
  profileOverride: ProfileAttributes
};

const convert = (value: number): string => `${Math.floor(value / 60)}:${value % 60 ? value % 60 : '00'}`;

export const ProfilePageAboutSection:
React.FC<ProfilePageAboutSectionProps> = ({ profileId, profileOverride }) => {
  const { t } = useTranslation('ProfilePageAboutSection');

  const [profile, profileError] = useObservable(
    () => (typeof profileOverride !== 'undefined' ? of(profileOverride) : social.service('profile').watch().get(`${profileId}`)), [profileId, profileOverride],
  );
  const [isPlaying, setIsPlaying] = useState(false);
  const [playedSeconds, setPlayedSeconds] = useState<number>(0);

  if (profileError) throw profileError;

  return (
    <>
      {profile === null && <Pending />}
      <Container>
        <div className="pt-3 pb-5">
          { profile?.profileAudio?.exists && (
          <>
            <ReactPlayer
              width="0"
              height="0"
              playsinline
              controls={false}
              config={{
                file: {
                  forceAudio: true,
                },
              }}
              playing={isPlaying}
              url={profile?.profileAudio?.downloadUrl}
              onPlay={() => setIsPlaying(true)}
              onPause={() => setIsPlaying(false)}
              onProgress={(e) => {
                setPlayedSeconds(e.playedSeconds);
              }}
            />
            <div
              style={{
                border: '1px solid #FFBF73', borderRadius: '25px', maxWidth: '370px',
              }}
            >
              <Row>

                <Col
                  xs={2}
                >
                  <Button
                    className="w-100 h-75 ml-1"
                    variant="outline-light"
                    onClick={() => setIsPlaying(!isPlaying)}
                  >
                    <Image
                      className="p-0"
                      style={{ height: '35px', width: '35px' }}
                      src={isPlaying ? Pause : Play}
                      alt="icon"
                    />
                  </Button>

                </Col>

                <Col className="d-flex align-items-center">
                  <p className="py-0 my-0"><strong>Welcome to my page</strong></p>
                </Col>

                <Col xs={2} className="d-flex align-items-center mr-2">
                  <p className="py-0 my-0"><small>{convert(Math.floor(playedSeconds))}</small></p>
                </Col>

              </Row>
            </div>

          </>
          )}
          <br />
          {profile?.aboutMessage && (
          <p className="text-muted">{profile.aboutMessage}</p>
          )}
          {!profile?.aboutMessage && (
          <p className="text-muted">{t('aboutMessageEmpty')}</p>
          )}
          {profile?.aboutHomepageUrl && (
          <p><Button variant="link" href={profile.aboutHomepageUrl}>{profile.aboutHomepageUrl}</Button></p>
          )}
          {(profile?.aboutTiktokUsername || profile?.aboutInstagramUsername) && (
          <p>
            { profile.aboutTiktokUsername && <Button title={t('visitTiktok', { profile })}>{profile.aboutTiktokUsername}</Button> }
            { profile.aboutInstagramUsername && <Button title={t('visitInstagram', { profile })}>{profile.aboutInstagramUsername}</Button> }
          </p>
          )}
          <p style={{ marginTop: '200px' }} />
        </div>

      </Container>
    </>
  );
};
export default ProfilePageAboutSection;
