import React from 'react';
import { RWebShare } from 'react-web-share';

export const WebShare: React.FC<{ title: string, text: string, url: string }> = (
  {
    title,
    text,
    url,
    children,
  },
) => (
  <RWebShare
    data={{
      text,
      title,
      url,
    }}
  >
    {children}
  </RWebShare>
);

export default WebShare;
