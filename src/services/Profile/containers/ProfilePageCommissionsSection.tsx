import { ProfileAttributes } from '@audiochills/api-social/src/attributes';
import React from 'react';
import { useTranslation } from 'react-i18next';

export type ProfilePageCommissionsSectionProps = {
  profileId: string
  profileOverride?: ProfileAttributes
};

export const ProfilePageCommissionsSection:
React.FC<ProfilePageCommissionsSectionProps> = () => {
  const { t } = useTranslation('ProfilePageCommissionsSection');
  return (
    <>
      {t('paragraph')}
    </>
  );
};
export default ProfilePageCommissionsSection;
