import React, { useCallback } from 'react';
import {
  Button, Card, Col, Row, Modal,
} from 'react-bootstrap';
import { ProfileAttributes } from '@audiochills/api-social/src/attributes';
import { useModalManager } from '@vlzh/react-modal-manager';
import { of } from 'rxjs';
import { useObservable } from 'react-use-observable';
import { social } from '../../../apis/social';
import AcLogo from '../images/acLogo.svg';

export type ProfileSubscribeModalProps = {
};

export type ProfileSubscribeModalParams = null | {
  profileId?: string
  profileOverride?: ProfileAttributes
};

export const ProfileSubscribeModal: React.FC<ProfileSubscribeModalProps> = () => {
  const {
    isOpen, closeModal, getParams,
  } = useModalManager('ProfileSubscribeModal');
  const params = getParams() as ProfileSubscribeModalParams;
  const { profileId, profileOverride } = params || { profileOverride: null };
  const [profile] = useObservable(
    () => (typeof profileOverride !== 'undefined' ? of(profileOverride) : social.service('profile').watch().get(`${profileId}`)),
    [profileId, profileOverride],
  );
  const [profileAvatar] = useObservable(
    () => (!profileId ? of(null) : social.service('profile-avatar').watch().get(`profileId:${profileId}`)),
    [profileId, profileOverride],
  );

  const [profileBanner] = useObservable(
    () => (!profileId ? of(null) : social.service('profile-banner').watch().get(`profileId:${profileId}`)),
    [profileId, profileOverride],
  );

  const getSubscriptionPortal = useCallback(async () => {
    const subscription = await (social.service('subscription') as any).create({ creatorId: profile!.id });
    window.location.href = subscription.url;
  }, [profile]);

  return (
    <>
      <Modal centered show={isOpen() && profile} onHide={() => closeModal()}>
        {profile && (
        <>
          <Modal.Header
            closeButton
            className="overflow-hidden"
            style={(profileBanner as any)?.exists
              ? { background: `no-repeat center/100% url(${profileBanner?.downloadUrl})`, height: '120px' }
              : { background: `no-repeat center/100% url(${AcLogo})`, height: '120px' }}
          />
          <Modal.Body
            className="pt-0"
            style={{ backgroundColor: '#FFF2E1' }}
          >
            <Row className="no-gutters">
              <Col xs="auto">
                <Card
                  style={{
                    border: '2px solid rgb(255,255,255)',
                    height: '120px',
                    marginLeft: '-2px',
                    marginRight: '-2px',
                    marginTop: '-60px',
                    width: '120px',
                  }}
                  className="overflow-hidden"
                >
                  <Card.Img
                    src={(profileAvatar as any)?.exists
                      ? profileAvatar?.downloadUrl
                      : AcLogo}
                    width={120}
                    height={120}
                  />
                </Card>
              </Col>
              <Col className="m-2">
                <p className="font-weight-bold mb-0">
                  {profile.name}
                </p>
                <p className="text-muted mb-0">
                  {`@${profile.username}`}
                </p>
              </Col>
            </Row>
            {/* <Trans i18nKey="subscribeText" t={t}> */}

            <p className="font-weight-bold mt-3 mb-2">
              Subscribe to get full access
            </p>
            <p className="text-muted mb-4">
              Subscription can be canceled anytime
            </p>

            {/* </Trans> */}
            <p className="text-center">
              <Button
                variant="primary"
                style={{ paddingBottom: '13px', paddingTop: '13px' }}
                onClick={getSubscriptionPortal}
                block
              >
                Subscribe

              </Button>
            </p>
          </Modal.Body>
        </>
        )}
      </Modal>
    </>
  );
};

export default ProfileSubscribeModal;
