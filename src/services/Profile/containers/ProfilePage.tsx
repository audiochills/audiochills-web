/* eslint-disable no-mixed-operators */
import React, { useMemo, useState } from 'react';
import { Trans, useTranslation } from 'react-i18next';
import {
  Button, Col, Container, Nav, Row,
} from 'react-bootstrap';
import { of } from 'rxjs';
import { LinkContainer } from 'react-router-bootstrap';
import { ParallaxBanner } from 'react-scroll-parallax';
import { useObservable, useRetryableObservable } from 'react-use-observable';
import { useAsyncFn } from 'react-use';
import SwipeableViews from 'react-swipeable-views';
import { Pending, social } from '../../../apis/social';
import { ProfilePageAboutSection } from './ProfilePageAboutSection';
import { ProfilePagePostsSection } from './ProfilePagePostsSection';
import { ReactComponent as ShareIcon } from '../images/share.svg';
import keycloak from '../../../keycloak';
import Avatar from '../components/Avatar';
import AvatarPlaceHolder from '../images/avatar-placeholder.png';
import BgPlaceHolder from '../images/bg-placeholder.png';
import { ReactComponent as VerifiedSvg } from '../images/verified.svg';
import WebShare from './WebShare';
import { eventNames, logEvent } from '../../../analytics';

// const ProfilePageAboutSection = React.lazy(() => import('./ProfilePageAboutSection'));
// const ProfilePagePostsSection = React.lazy(() => import('./ProfilePagePostsSection'));

export type ProfilePageProps = {
  profileUsername: string
};

export const ProfilePage: React.FC<ProfilePageProps> = (props) => {
  // eslint-disable-next-line no-console
  const { t } = useTranslation('ProfilePage');
  useTranslation('PostItem');

  const [authentication] = useObservable(() => social.authentication.result(), []);

  const [profile, profileError] = useObservable(
    () => social.service('profile').watch().get(`username:${props.profileUsername}`), [props.profileUsername],
  );

  const [stripeInfo, stripeInfoError] = useObservable(
    () => (!profile
      ? of(profile)
      : social.service('stripe-info').watch().get(`profileId:${profile.id}`)),
    [profile],
  );

  const shouldLoadProfileFollowCreator = useMemo(
    () => (profile && authentication && authentication.profile.id !== profile.id),
    [profile, authentication],
  );

  const [profileFollowCreator,
    profileFollowCreatorError,,
    retryProfileFollowCreator] = useRetryableObservable(
    () => (shouldLoadProfileFollowCreator
      ? social.service('profile-follow-profile').watch().find({ query: { followId: profile && profile.id, followerId: authentication && authentication.profile.id } })
      : of(null)
    ),
    [profile, authentication],
  );

  const [followProfileState, followProfile] = useAsyncFn(
    async () => {
      if (!profile || !authentication) return;
      await social.service('profile-follow-profile').create({ followId: profile.id, followerId: authentication.profile.id });
      retryProfileFollowCreator();
      logEvent(eventNames.FOLLOW_CREATOR, {
        creatorUsername: profile.username,
        userAudience: authentication.profile.followerCount,
      });
    },
    [profile && profile.id, authentication && authentication.profile.id],
  );
  const [unfollowProfileState, unfollowProfile] = useAsyncFn(
    async () => {
      if (!profile
        || !authentication
        || !profileFollowCreator
        || !profileFollowCreator.data.length
      ) return;
      await social.service('profile-follow-profile').remove(profileFollowCreator.data[0].id);
      retryProfileFollowCreator();
    },
    [
      profile && profile.id,
      authentication && authentication.profile.id,
      profileFollowCreator && profileFollowCreator.data.length && profileFollowCreator.data[0].id,
    ],
  );

  const [tabKey, setTabKey] = useState(1);

  if (profileError) throw profileError;
  if (stripeInfoError) throw stripeInfoError;
  if (profileFollowCreatorError) throw profileFollowCreatorError;

  return (
    <>

      {profile === null && <Pending />}
      {stripeInfo === null && <Pending />}
      {shouldLoadProfileFollowCreator && profileFollowCreator === null && <Pending />}

      <ParallaxBanner
        disabled={false}
        layers={[
          {
            amount: 0.5,
            image: (profile?.profileBanner as any)?.exists
              ? profile?.profileBanner?.downloadUrl
              : BgPlaceHolder,
          },
        ]}
        style={{
          height: '200px', objectFit: 'cover', overflow: 'hidden',
        }}
      />
      <Container>

        <Row className="no-gutters">
          <Col xs="auto" className="">
            <div style={{ display: 'inline-block', marginTop: 'calc(-160px / 2)' }}>
              <div style={{ display: 'inline-block', margin: 'calc(-4px)' }}>
                <Avatar src={(profile?.profileAvatar as any)?.exists
                  ? profile?.profileAvatar?.downloadUrl
                  : AvatarPlaceHolder}
                />
              </div>
            </div>
          </Col>
          <Col className="m-2">
            {(shouldLoadProfileFollowCreator && profileFollowCreator) && (
              <Button
                variant="outline-primary"
                className="btn-block"
                onClick={profileFollowCreator.data.length ? unfollowProfile : followProfile}
                disabled={unfollowProfileState.loading || followProfileState.loading}
              >
                {!profileFollowCreator.data.length
                  ? t('followButtonText', { defaultValue: 'Follow' })
                  : t('unFollowButtonText', { defaultValue: 'Unfollow' })}

              </Button>
            )}
            {/* {(shouldLoadProfileFollowCreator && !profileFollowCreator) && (
              <LinkContainer to={generatePath('/profile-settings')}>
                <Button
                  variant="outline-primary"
                  className="btn-block"
                >
                  {t('editButtonText')}
                </Button>
              </LinkContainer>
            )} */}

            {!shouldLoadProfileFollowCreator
            && (profile?.id !== (authentication as any)?.profile?.id) && (
              <Button variant="outline-primary" className="btn-block" onClick={() => keycloak.login()}>{t('signinAndFollowButtonText', { defaultValue: 'Signin and Follow' })}</Button>
            )}
          </Col>
          <Col className="mt-2" xs="auto">
            <WebShare
              title="Audiochills"
              text=""
              url={`https://audiochills.art/profile/${profile?.username}`}
            >
              <Button variant="outline-link" className="text-primary p-0">
                <ShareIcon style={{ height: '33px', width: '33px' }} />
              </Button>
            </WebShare>
          </Col>
        </Row>
      </Container>
      <Container>
        <p className="font-weight-bold mb-0">
          {profile?.name}
          {stripeInfo?.detailsSubmitted && stripeInfo?.chargesEnabled
              && stripeInfo?.payoutsEnabled && (<VerifiedSvg style={{ marginBottom: '4px' }} />) }
        </p>
        {' '}
        <p className="text-muted">
          {`@${profile?.username}`}
        </p>
        <p>
          <Trans i18nKey="followerCount" count={profile?.followerCount}>
            <strong>{{ followerCount: profile?.followerCount }}</strong>
            {' '}
            Followers
          </Trans>
        </p>
        <p>
          {profile?.categories?.map((category) => (
            <React.Fragment key={category.id}>
              <LinkContainer to={`/explore/${category.slug}`}>
                <Button style={{ backgroundColor: '#ECF1F4' }} variant="light" className="btn-sm ">{`#${category.name}`}</Button>
              </LinkContainer>
              {' '}
            </React.Fragment>
          ))}
        </p>
      </Container>
      <Container>
        <Nav variant="tabs" onSelect={(k) => setTabKey(parseInt(`${k}`, 10))} activeKey={tabKey}>
          <Nav.Item>
            <Nav.Link eventKey={1}>{t('posts')}</Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link eventKey={0}>{t('about')}</Nav.Link>
          </Nav.Item>
          {/* <Nav.Item>
              <Nav.Link disabled eventKey="commisions">{t('commisions')}</Nav.Link>
            </Nav.Item> */}
        </Nav>
      </Container>
      <SwipeableViews index={tabKey} onChangeIndex={(k) => setTabKey(k)}>
        {profile && (
        <div>
          <ProfilePageAboutSection profileId={profile.id} profileOverride={profile} />
        </div>
        )}
        {profile && (
        <div>
          <ProfilePagePostsSection profileId={profile.id} profileOverride={profile} />
        </div>
        )}
      </SwipeableViews>
    </>
  );
};
export default ProfilePage;
