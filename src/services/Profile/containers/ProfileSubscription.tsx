import React, { useCallback, useState } from 'react';
import {
  Button, Col, Container, Row,
} from 'react-bootstrap';
import { useObservable } from 'react-use-observable';
import { of } from 'rxjs';
import CurrencyInput from 'react-currency-input-field';
import { Link } from 'react-router-dom';
import { Pending, social } from '../../../apis/social';

export const ProfileSubscription: React.FC = () => {
  // eslint-disable-next-line no-console

  const [currencyError, setCurrencyError] = useState<string | null | undefined>(null);
  const [currencyVal, setCurrencyVal] = useState<number>(0);
  const [authentication] = useObservable(() => social.authentication.result(), []);

  const [profile] = useObservable(
    () => (!authentication ? of(null) : social.service('profile').watch().get(authentication.profile.id)),
    [authentication && authentication.profile.id],
  );
  const [stripeInfo] = useObservable(
    () => (!authentication ? of(null) : social.service('stripe-info').watch().get(`profileId:${authentication.profile.id}`)),
    [authentication && authentication.profile.id],
  );
  const [plan] = useObservable(
    () => (!authentication ? of(null) : social.service('plan').watch().get(`profileId:${authentication.profile.id}`)),
    [authentication && authentication.profile.id],
  );

  const isCreator = useCallback(() => (stripeInfo?.chargesEnabled
           && stripeInfo?.detailsSubmitted
           && stripeInfo?.payoutsEnabled), [stripeInfo, profile]);

  const updateButton = useCallback((e: number) => {
    if (e < 4) {
      setCurrencyError('Subscription price too small');
      setCurrencyVal(0);
    } else if (e > 1_000_000) {
      setCurrencyError('Subscrption price too high');
      setCurrencyVal(0);
    } else {
      setCurrencyError(null);
      setCurrencyVal(e);
    }
  }, []);

  const updateProfile = useCallback(async () => {
    if (!plan) return;
    await social.service('plan').patch(plan.id, { amountCents: currencyVal * 100 });
  }, [plan, profile, currencyVal]);

  if (typeof authentication === 'undefined') return <Pending />;

  if (!profile) return <Pending />;

  return (
    <Container>
      {
      authentication
      && (
        <>
          <br />
          <strong>Monetisation</strong>
          <br />
          <br />

          <Row className="mt-3">

            <Col xs={9}>
              <div>
                <strong>Price monthly subscription</strong>
                <p>
                  Define a price that allows users access to your content for 30 days
                </p>
              </div>

            </Col>

            <Col>
              <CurrencyInput
                max={1000000}
                className="mt-3 text-center "
                id="input-example"
                name="input-name"
                placeholder="5.99"
                prefix="$"
                min={5}
                step={1}
                disabled={!isCreator()}
                style={{
                  borderColor: 'grey', borderRadius: '4px', borderStyle: 'solid', borderWidth: '0.3px', width: '68px',
                }}
                defaultValue={(plan?.amountCents ?? 0) / 100}
                decimalsLimit={2}
                onValueChange={(e) => updateButton(parseInt((e ?? '0'), 10))}
              />
            </Col>
          </Row>

          {
            !isCreator() && (
              <small>
                <strong>
                  Please apply for becoming a creator before changing your subscription settings
                  <Link to="/become-creator">

                    <u className="text-success">
                      {' '}
                      {' '}
                      {' '}
                      over here

                    </u>
                  </Link>
                </strong>

              </small>
            )
          }

          {
          plan?.amountCents
          && (
          <small>
            {' '}
            <strong>
              Current price:
              {' '}
              {' $'}
              {plan!.amountCents / 100}
            </strong>
            {' '}

          </small>
          )
          }
          <br />
          {
          currencyError
          && (
          <small>
            {' '}
            <strong>
              {currencyError}
            </strong>
            {' '}

          </small>
          )
          }

          {/* <LinkContainer to="/profile-settings" exact style={{ marginTop: '250px' }}> */}
          <Button
            className="py-3"
            block
            disabled={currencyVal < 4}
            onClick={updateProfile}
            style={{ marginTop: '250px' }}
          >
            {' '}
            save changes

          </Button>

          {/* </LinkContainer> */}

        </>
      )

  }

    </Container>
  );
};
export default ProfileSubscription;
