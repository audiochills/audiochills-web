import React, { useCallback } from 'react';
import {
  Button, Card, Col, Row, Modal,
} from 'react-bootstrap';
import { PostAttributes, PostAudioAttributes } from '@audiochills/api-social/src/attributes';
import { useModalManager } from '@vlzh/react-modal-manager';
import { of } from 'rxjs';
import { useObservable } from 'react-use-observable';
import { social } from '../../../apis/social';
import AcLogo from '../images/acLogo.svg';

export type UnlockPostModalProps = {
};

export type UnlockPostModalParams = null | {
  postId?: string
  postOverride?: PostAttributes
  postAudio?: PostAudioAttributes
};

export const UnlockPostModal: React.FC<UnlockPostModalProps> = () => {
  const {
    isOpen, closeModal, getParams,
  } = useModalManager('UnlockPostModal');
  const params = getParams() as UnlockPostModalParams;
  const { postId, postOverride } = params || { audio: null, postOverride: null };

  const [post] = useObservable(
    () => (typeof postOverride !== 'undefined' ? of(postOverride) : social.service('post').watch().get(`${postId}`)),
    [postId, postOverride],
  );
  const [profileAvatar] = useObservable(
    () => (!post?.id ? of(null) : social.service('profile-avatar').watch().get(`profileId:${postOverride?.authorId}`)),
    [post?.id],
  );

  const [profileBanner] = useObservable(
    () => (!post?.id ? of(null) : social.service('profile-banner').watch().get(`profileId:${postOverride?.authorId}`)),
    [post?.id],
  );
  const stripeCheckout = useCallback(async () => {
    const stripeUrl = await (social.service('purchase-post') as any).patch(null, { postId: post?.id ?? '' });
    window.location.href = stripeUrl.url;
  }, [post]);

  return (
    <>
      <Modal centered show={isOpen() && post} onHide={() => closeModal()}>
        {post && (
        <>
          <Modal.Header
            closeButton
            className="overflow-hidden"
            style={(profileBanner as any)?.exists
              ? { background: `no-repeat center/100% url(${profileBanner?.downloadUrl})`, height: '120px' }
              : { background: `no-repeat center/100% url(${AcLogo})`, height: '120px' }}
          />
          <Modal.Body className="pt-0 pb-2" style={{ backgroundColor: '#FFF2E1' }}>
            <Row className="no-gutters">
              <Col xs="auto">
                <Card
                  style={{
                    border: '2px solid rgb(255,255,255)',
                    height: '120px',
                    marginLeft: '-2px',
                    marginRight: '-2px',
                    marginTop: '-60px',
                    width: '120px',
                  }}
                  className="overflow-hidden"
                >
                  <Card.Img
                    src={(profileAvatar as any)?.exists
                      ? profileAvatar?.downloadUrl
                      : AcLogo}
                    width={120}
                    height={120}
                  />
                </Card>
              </Col>
              <Col className="m-2">
                <p className="font-weight-bold mb-0">
                  {post?.author?.name}
                </p>
                <p className="text-muted mb-0">
                  {`@${post?.author?.username}`}
                </p>
              </Col>
            </Row>

            <p className="font-weight-bold mt-3 mb-2">
              unlock to get full access
            </p>
            <p className="text-muted mb-4">
              You will have permanent access to this creation
            </p>

            <p className="text-center mt-2">
              <strong>
                <Button
                  className=" px-3"
                  style={{ paddingBottom: '13px', paddingTop: '13px' }}
                  variant="primary"
                  block
                  onClick={stripeCheckout}
                >
                  Unlock for $
                  {(post?.price ?? 0) / 100}
                </Button>
              </strong>
            </p>

          </Modal.Body>
        </>
        )}
      </Modal>
    </>
  );
};

export default UnlockPostModal;
