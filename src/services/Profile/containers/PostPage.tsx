import React, { useCallback, useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import {
  Button, Card, Col, Row, Container, Form, Badge,
} from 'react-bootstrap';
import { PostAttributes } from '@audiochills/api-social/src/attributes';
import { useObservable, useRetryableObservable } from 'react-use-observable';
import Moment from 'react-moment';
import { generatePath, Link } from 'react-router-dom';
import moment from 'moment';
import { useModalManager } from '@vlzh/react-modal-manager';
import { of } from 'rxjs';
import { useAsyncFn, usePreviousDistinct } from 'react-use';
import { social, Pending } from '../../../apis/social';
import { ReactComponent as PlayIcon } from '../images/play.svg';
import { CommentItem } from './CommentItem';
import BgPlaceHolder from '../images/bg-placeholder.png';
import LockSvg from '../images/lock.svg';
import { ReactComponent as VerifiedSvg } from '../images/verified.svg';
import { ReactComponent as CommentIcon } from '../images/reactionComment.svg';
import { ReactComponent as HeartIcon } from '../images/reactionHeart.svg';
import { ReactComponent as TipIcon } from '../images/reactionTip.svg';
import { ReactComponent as ShareIcon } from '../images/reactionShare.svg';

import WebShare from './WebShare';
import { eventNames, logEvent } from '../../../analytics';
import keycloak from '../../../keycloak';

export type PostPageProps = {
  postId: string
  postOverride?: PostAttributes
};
const ReactionsGrey = { color: 'darkgray ' };

export const PostPage: React.FC<PostPageProps> = (props) => {
  const { t } = useTranslation('PostPage');
  useTranslation('CommentItem');
  useTranslation('SubcommentItem');

  const [currentComments, , , retryComments] = useRetryableObservable(
    () => social.service('comment').watch().find({
      query: {
        $sort: {
          createdAt: -1,
        },
        postId: props.postId,
      },
    }), [props.postId],
  );
  const prevComments = usePreviousDistinct(currentComments, (prev, next) => !!prev && !next);
  const comments = useMemo(() => currentComments || prevComments, [currentComments, prevComments]);

  const [currentPost, , , retryPost] = useRetryableObservable(
    () => social.service('post').watch().get(props.postId), [props.postId],
  );
  const prevPost = usePreviousDistinct(currentPost, (prev, next) => !!prev && !next);
  const post = useMemo(
    () => currentPost || prevPost,
    [currentPost, prevPost],
  );

  const [authentication] = useObservable(() => social.authentication.result(), []);
  const [postImage] = useObservable(
    () => social.service('post-image').watch().get(`postId:${props.postId}`),
    [props.postId],
  );
  const [postAudio] = useObservable(
    () => (social.service('post-audio').watch().get(`postId:${props.postId}`)),
    [props.postId],
  );
  const [stripeInfo] = useObservable(
    () => social.service('stripe-info').watch().get(`profileId:${post?.authorId}`),
    [post && post.authorId],
  );
  const [profileLikePost] = useObservable(
    () => (!authentication
      ? of(null)
      : (social.service('profile-like-post').watch().find({ query: { postId: props.postId, profileId: authentication.profile.id } }))),
    [props.postId, authentication && authentication.profile.id],
  );
  const descriptionIsTooLong = React.useMemo(
    () => post && post.description.length > 100,
    [post],
  );
  const [descriptionIsTruncated, setDescriptionIsTruncated] = React.useState(true);
  const toggleDescriptionIsTruncated = React.useCallback(
    () => setDescriptionIsTruncated(!descriptionIsTruncated),
    [descriptionIsTruncated],
  );
  const truncatedDescription = React.useMemo(
    () => post?.description.slice(0, 100),
    [post],
  );

  const [createCommentSubmitState, createCommentSubmit] = useAsyncFn(
    async (e) => {
      e.preventDefault();
      if (!authentication) return keycloak && await keycloak.login();
      if (!post) return undefined;
      await social.service('comment').create({ authorId: authentication.profile.id, postId: post.id, ...Object.fromEntries(new FormData(e.target)) });
      logEvent(eventNames.COMMENT_MADE, {
        category: post?.genre?.category?.slug,
        creatorUsername: post?.author?.username,
        genre: post?.genre?.id,
        postId: post?.id,
      });
      e.target.reset();
      retryComments();
      return undefined;
    },
    [post, authentication],
  );
  const { openModal } = useModalManager();

  const [manageLikeState, manageLike] = useAsyncFn(async () => {
    if (!authentication) return keycloak && await keycloak.login();
    if (!profileLikePost) return undefined;
    if (!post) return undefined;
    if (profileLikePost.data.length) {
      await social.service('profile-like-post').remove(profileLikePost.data[0].id);
    } else {
      await social.service('profile-like-post').create({ postId: post.id, profileId: authentication.profile.id });
    }
    retryPost();
    return undefined;
  }, [post, profileLikePost, authentication]);

  const playAudio = useCallback(() => {
    if (!post) return;
    openModal('BottomPlayerModal', true, { post });
    logEvent(eventNames.PLAY_CREATION, {
      amountTips: ((post as any)?.tipTotalCents ?? 0) / 100,
      category: post.genre?.category?.slug,
      comments: '',
      creatorUsername: post.author?.username,
      following: authentication ? authentication?.profile?.followerCount : -1,
      genre: post.genreId,
      likes: post.likeCount,
      monetisation: post.isPaid ? 'Paid' : 'Free',
      price: (post.price || 0) / 100,
      tags: post.customTags,
      userAudience: post.author?.followerCount,
    });
  }, [post]);

  const playOrPay = useCallback(async () => {
    if ((postAudio as any)?.isAuthorized) {
      // eslint-disable-next-line no-console
      console.log('play');
      playAudio();
    } else {
      if (!authentication) return keycloak && await keycloak.login();
      openModal('UnlockPostModal', false, { postId: post!.id, postOverride: post });
    }
    return undefined;
  }, [post, postAudio]);

  const openTipModal = useCallback(() => {
    if (!authentication) return keycloak && keycloak.login();
    openModal('TipPostModal', false, { postId: post?.id, postOverride: post });
    return undefined;
  }, [post]);

  if (!comments || !post) return <Pending />;

  return (
    <>
      <Container>
        <Row className="my-2">
          <Col xs="auto">
            <small className="font-weight-bold align-text-bottom">
              {/* {post.author?.username} */}
              {post.author?.username && (
                <Link to={generatePath('/profile/:profileUsername', { profileUsername: post.author?.username })} className="text-dark">{ post.author?.username }</Link>
              )}
              {' '}
              {stripeInfo?.detailsSubmitted && stripeInfo?.chargesEnabled
              && stripeInfo?.payoutsEnabled && (<VerifiedSvg style={{ marginBottom: '4px' }} />) }
              {' '}
              {/* {post.author?.isNsfw && (
                <Badge variant="secondary">{t('nsfwBadge')}</Badge>
              )} */}
            </small>
          </Col>
          <Col xs="auto" className="ml-auto">
            <small style={ReactionsGrey} title={moment(post.createdAt).toString()}>
              <Moment fromNow ago date={moment(post.createdAt)} />
              {' '}
              ago
              {' '}
              {/* <Button variant="link" size="sm" className="text-black-50 p-0">
              <FontAwesomeIcon icon={faEllipsisH} /></Button> */}
            </small>
          </Col>
        </Row>
        <Row className="mb-2">
          <Col xs="12">

            <Button
              onClick={playOrPay}
              className="p-0"
              style={{
                backgroundColor: 'transparent', border: 'none', height: '100%', width: '100%',
              }}
            >
              <Card
                onClick={playOrPay}
              >
                {/* <Card.Img variant="top" width="170" src={post.author?.profileAvata
                r?.downloadUrl || avatar} title={t('profileimage', { post })} /> */}
                <Card.Img
                  variant="top"
                  style={{
                    borderRadius: '15px',
                    maxHeight: '330px',
                    minHeight: '331px',
                    minWidth: '100%',
                    objectFit: 'cover',
                    overflow: 'hidden',
                  }}
                  src={(postImage as any)?.exists
                    ? (postImage as any)?.downloadUrl
                    : BgPlaceHolder}
                  title={t('profileimage', { post })}
                />
                {
                  !(postAudio as any)?.isAuthorized
                  && (
                    <Card.Img
                      className="p-4"
                      style={{
                        backgroundColor: 'rgba(255, 255, 255, 0.6)',
                        height: '100%',
                        left: '0',
                        position: 'absolute',
                        top: '0',
                        width: '100%',
                      }}
                      src={LockSvg}
                    />
                  )
                }
              </Card>

            </Button>

          </Col>
          <Col className="mt-2 pb-0">
            <p className="text-black-50 mb-0">

              <small>
                <span style={ReactionsGrey}>{post.genre?.category?.name}</span>
                {' '}
                /
                {' '}
                <span style={ReactionsGrey}>{post.genre?.name}</span>
              </small>

            </p>
            <p className="mb-0"><Link to={`/post/${post.id}`} className="font-weight-bold text-dark">{post.title}</Link></p>
          </Col>
          <Col xs="auto" className="mt-3">
            <p className="mb-0">
              <a href="#play" title={t('play')} className="text-primary" onClick={playOrPay}>
                <PlayIcon />
              </a>
            </p>
            {/* <p className="text-center text-muted"><small>12:23</small></p> */}
          </Col>

          {
            post?.customTags?.length
            && (
            <Col className="pt-1" xs={12}>
              <p>
                {
            post?.customTags?.map((e) => (
              <Badge
                className="py-2 px-2 text-muted border bg-light mr-2 ml-1"
                style={{ borderRadius: '5px', fontWeight: 'normal' }}
              >
                {e}
              </Badge>
            ))
        }
              </p>
            </Col>
            )
          }

        </Row>

        <p className="mb-2">{descriptionIsTooLong && descriptionIsTruncated ? truncatedDescription : post.description}</p>
        {descriptionIsTooLong && (
          <p>
            <small>
              <a className="text-muted" href={descriptionIsTruncated ? '#more' : '#less'} onClick={toggleDescriptionIsTruncated}>
                {descriptionIsTruncated && t('more')}
                {!descriptionIsTruncated && t('less')}
              </a>
            </small>

          </p>
        )}
        <p className="mb-3">

          <Button
            variant="link"
            size="sm"
            className="mr-1"
            onClick={manageLike}
            disabled={manageLikeState.loading}
            style={{ color: ((profileLikePost as any)?.data?.length) ? 'red' : 'darkgray' }}
          >
            {/* <FontAwesomeIcon icon={faHeart}
              color={(profileLikePost as any)?.data?.length > 0 ? 'red' : 'grey'} /> */}

            <HeartIcon />
            {' '}
            {post.likeCount}
          </Button>

          <Link
            className="mx-1"
            to={`/post/${post?.id}`}
            style={ReactionsGrey}
          >
            <CommentIcon />
            {' '}
            {post.commentCount}

          </Link>

          {stripeInfo?.detailsSubmitted && stripeInfo?.chargesEnabled
              && stripeInfo?.payoutsEnabled && (
              <Button
                className="mx-1"
                variant="link"
                size="sm"
                style={ReactionsGrey}
                onClick={openTipModal}
              >
                {/* <FontAwesomeIcon icon={faCoins} /> */}
                <TipIcon />
                {' '}
                <small>$</small>
                {((post as any)?.tipTotalCents ?? 0) / 100}
              </Button>
          ) }

          <WebShare
            title="Audiochills"
            text=""
            url={`https://audiochills.art/post/${post?.id}`}
          >
            <Button
              className="mx-1"
              variant="link"
              size="sm"
              style={ReactionsGrey}
            >
              <ShareIcon />
            </Button>
          </WebShare>

        </p>
        {comments.data.map((comment, idx, data) => (
          <CommentItem
            key={comment.id}
            commentOverride={comment}
            commentId={comment.id}
            disableLine={idx + 1 < data.length}
          />
        ))}

        <Form onSubmit={createCommentSubmit}>
          <Row className="no-gutters border pt-3 px-3 mb-2 rounded">
            <Col>
              <p>
                <Form.Control
                  size="sm"
                  type="text"
                  placeholder={t('commentMessagePlaceholder')}
                  required
                  name="message"
                />
              </p>
            </Col>
            <Col xs="auto" className="ml-2">
              <p>
                <Button size="sm" variant="primary" type="submit" disabled={createCommentSubmitState.loading}>
                  {t('submitComment')}
                </Button>
              </p>
            </Col>
          </Row>
        </Form>

        <p style={{ marginTop: '100px' }} />
      </Container>
    </>
  );
};

export default PostPage;
