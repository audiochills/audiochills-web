import React from 'react';
import { Card } from 'react-bootstrap';

type AvatarProps = {
  src?: string
  alt?: string
  title?: string
  height?: string
  width?: string
  border?: string
  borderWidth?: string
  className?: string
};

const Avatar: React.FC<AvatarProps> = ({
  src, alt, title, width = '160px', border = 'solid rgb(255,255,255)', borderWidth = '4px', className = '',
}) => (
  <Card
    style={{
      border: `${border}`,
      borderWidth: `calc(${borderWidth})`,
      maxWidth: '100%',
      position: 'relative',
      width: `calc(${width} + ${borderWidth} * 2)`,
    }}
    className={`overflow-hidden ${className}`}
  >
    {src && (
      <Card.Img src={src} style={{ borderRadius: '18px', width: '100%' }} alt={alt} title={title} />
    )}
  </Card>
);
export default Avatar;
