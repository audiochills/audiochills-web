import React from 'react';
import { Route } from 'react-router-dom';
import AddPaymentModal from './containers/AddPaymentModal';
import ProfileSubscribeModal from './containers/ProfileSubscribeModal';
import ProfileSettingsImageModal from './containers/ProfileSettingsImageModal';
import TipPostModal from './containers/TipPostModal';
import UnlockPostModal from './containers/UnlockPostModal';
import ProfileSubscription from './containers/ProfileSubscription';
import SharePostModal from './containers/SharePostModal';
import ShareProfileModal from './containers/ShareProfileModal';
import TopBar from '../PostCreation/containers/TopBar';

const ProfilePage = React.lazy(() => import('./containers/ProfilePage'));
const PostPage = React.lazy(() => import('./containers/PostPage'));
const ProfileSettingsPage = React.lazy(() => import('./containers/ProfileSettingsPage'));

export const Modals: React.FC<{}> = () => (
  <>
    <ProfileSubscribeModal />
    <ProfileSettingsImageModal />
    <AddPaymentModal />
    <TipPostModal />
    <UnlockPostModal />
    <SharePostModal />
    <ShareProfileModal />
  </>
);
export default () => [
  <Route exact path="/profile/:profileUsername" render={({ match }) => <ProfilePage profileUsername={match.params.profileUsername!} />} />,
  <Route exact path="/profile-settings" render={() => <ProfileSettingsPage />} />,
  <Route exact path="/profile-settings/subscription" render={() => <ProfileSubscription />} />,
  <Route
    exact
    path="/post/:postId"
    render={({ match }) => (
      <TopBar progress={0} title="Post Details" navigateBackTo="/">
        {' '}
        <PostPage postId={match.params.postId!} key={match.params.postId!} />
        {' '}
      </TopBar>
    )}
  />,
];
