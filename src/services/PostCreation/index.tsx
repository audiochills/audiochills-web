import React from 'react';

// eslint-disable-next-line import/no-named-as-default
import { Route } from 'react-router-dom';
import Tags from './containers/Category/Tags';

import { TopBar } from './containers/TopBar';

const Title = React.lazy(() => import('./containers/General/Title'));
const CategoryGenre = React.lazy(() => import('./containers/Category/CategoryGenre'));
const CategoryInfo = React.lazy(() => import('./containers/Category/CategoryInfo'));
const Collaborators = React.lazy(() => import('./containers/Category/Collaborators'));
const Description = React.lazy(() => import('./containers/General/Description'));
const GeneralInfo = React.lazy(() => import('./containers/General/GeneralInfo'));
// const Tags = React.lazy(() => import('./containers/Category/Tags'));
const Publish = React.lazy(() => import('./containers/Publish/Publish'));
const Review = React.lazy(() => import('./containers/Category/Review'));

export default () => [
  <Route
    exact
    path="/post/general/:postId?"
    render={({ match }) => (
      <TopBar progress={33.3} title="Post creation" navigateBackTo="/">
        {' '}
        <GeneralInfo postId={match.params.postId} key={match.params.postId} />
        {' '}
      </TopBar>
    )}
  />,
  <Route
    exact
    path="/post/title/:postId"
    render={({ match }) => (
      <TopBar progress={0} title="Title" postId={match.params.postId} navigateBackTo="/post/general">
        {' '}
        <Title postId={match.params.postId} key={match.params.postId} />
        {' '}
      </TopBar>
    )}
  />,
  <Route
    exact
    path="/post/description/:postId"
    render={({ match }) => (
      <TopBar progress={0} title="Description" postId={match.params.postId} navigateBackTo="/post/general">
        {' '}
        <Description postId={match.params.postId} key={match.params.postId} />
        {' '}
      </TopBar>
    )}
  />,
  <Route
    exact
    path="/post/category-info/:postId"
    render={({ match }) => (
      <TopBar progress={66.6} title="Post creation" postId={match.params.postId} navigateBackTo="/post/general">
        {' '}
        <CategoryInfo postId={match.params.postId} key={match.params.postId} />
        {' '}
      </TopBar>
    )}
  />,
  <Route
    exact
    path="/post/review/:postId"
    render={({ match }) => (
      <TopBar progress={66.6} title="Post creation" postId={match.params.postId} navigateBackTo="/post/general">
        {' '}
        <Review postId={match.params.postId} key={match.params.postId} />
        {' '}
      </TopBar>
    )}
  />,
  <Route
    exact
    path="/post/collaborators/:postId"
    render={({ match }) => (
      <TopBar progress={0} title="Collaborators" postId={match.params.postId} navigateBackTo="/post/category-info">
        {' '}
        <Collaborators postId={match.params.postId} key={match.params.postId} />
        {' '}
      </TopBar>
    )}
  />,
  <Route
    exact
    path="/post/category-genre/:postId"
    render={({ match }) => (
      <TopBar progress={0} title="Category / Genre" postId={match.params.postId} navigateBackTo="/post/category-info">
        {' '}
        <CategoryGenre postId={match.params.postId} key={match.params.postId} />
        {' '}
      </TopBar>
    )}
  />,
  <Route
    exact
    path="/post/tags/:postId"
    render={({ match }) => (
      <TopBar progress={0} title="User defined tags" postId={match.params.postId} navigateBackTo="/post/category-info">
        {' '}
        <Tags postId={match.params.postId} key={match.params.postId} />
        {' '}
      </TopBar>
    )}
  />,
  <Route
    exact
    path="/post/publish/:postId"
    render={({ match }) => (
      <TopBar progress={99.9} title="Post creation" postId={match.params.postId} navigateBackTo="/post/category-info">
        {' '}
        <Publish postId={match.params.postId} key={match.params.postId} />
        {' '}
      </TopBar>
    )}
  />,
];
