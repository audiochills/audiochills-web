import { of, Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { fromStream, fromPromise, IPromiseBasedObservable } from 'mobx-utils';

/**
 * Converts a rxjs Observable Stream into a Mobx Observable Stream Object,
 * filled with Mobx Observable Promises. Internally it uses
 * {@link https://mamaya-spark.gitbooks.io/mobx-guide/content/mobx-utils/fromstream.html|fromStream} and
 * {@link https://mamaya-spark.gitbooks.io/mobx-guide/content/mobx-utils/frompromise.html|fromPromise}.
 *
 * @param createStream A function which creates the stream.
 * @param override An override value, which will be wrapped and returned instead of creating.
 * @return
 * An object which stores the observable promise, which has a `case()` function, as `current`.
 * @example
 * const store = fromServiceStream(() => social.service('category').watch().get('music'))
 * const name = store.current.case({ fulfilled: ({ name }) => name })
 */
export default function fromServiceStream<T>(createStream: () => Observable<T>, override?: T) {
  if (override) {
    return fromStream(of<IPromiseBasedObservable<T>>(), fromPromise(Promise.resolve(override)));
  }
  return fromStream(
    createStream()
      .pipe(map((d) => fromPromise(Promise.resolve(d))))
      .pipe(catchError((e) => of(fromPromise(Promise.reject<T>(e))))),
    fromPromise(new Promise<T>(() => undefined)),
  );
}
