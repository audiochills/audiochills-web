import { observer } from 'mobx-react';
import React, { useCallback, useRef, useState } from 'react';
import {
  Container, Row, Col, Button, Form,
} from 'react-bootstrap';
import { generatePath, useHistory } from 'react-router-dom';

import CurrencyInput from 'react-currency-input-field';
import { useObservable } from 'react-use-observable';
import { of } from 'rxjs';
import {
  social, Pending,
} from '../../../../apis/social';
import { eventNames, logEvent } from '../../../../analytics';

export type PublishProps = {
  postId: string
};

export const Publish: React.FC<PublishProps> = observer(({ postId }) => {
  const [enableSubmit, setEnableSubmit] = useState(true);

  const checkRef = useRef<any>(undefined);
  const priceRef = useRef<any>(undefined);
  const history = useHistory();

  const [authentication] = useObservable(() => social.authentication.result(), []);

  const [stripeInfo] = useObservable(
    () => (!authentication ? of(null) : social.service('stripe-info').watch().get(`profileId:${authentication.profile.id}`)),
    [authentication && authentication?.profile?.id],
  );

  const [post] = useObservable(
    () => (!authentication ? of(null) : social.service('post').watch().get(postId)),
    [authentication && authentication?.profile?.id, postId],
  );

  const navigateTo = useCallback((pathName: string) => {
    if (!authentication) return;
    history.push({
      pathname: generatePath(`${pathName}/:username`, { username: authentication.profile.username }),
    });
  }, [authentication, postId]);

  const updatePost = useCallback(() => {
    setEnableSubmit(false);

    const val = Number((priceRef.current.value.replace('$', '')).replace(',', ''));
    const isPaid = checkRef.current.checked as boolean;
    const price = (isPaid ? val : 0) * 100;
    const isPublished = true;

    social.service('post').patch(postId, { isPaid, isPublished, price })
      .then(() => {
        logEvent(eventNames.PUBLISHED_CREATION, {
          category: post?.genre?.category?.slug,
          creatorUsername: post?.author?.username,
          genre: post?.genreId,
          monetisation: isPaid ? 'Paid' : 'Free',
          price: price / 100,
          tags: post?.customTags?.join(' , '),
        });
        navigateTo('/profile');
      })
      .catch(() => {
        setEnableSubmit(true);
      });
  }, [authentication, postId]);

  const updateButton = useCallback(() => {
    const val = Number((priceRef.current.value.replace('$', '')).replace(',', ''));

    setEnableSubmit(
      (checkRef?.current.checked && val > 2)
      || !checkRef?.current.checked,
    );
  }, []);

  const isCreator = useCallback(() => (stripeInfo?.chargesEnabled
           && stripeInfo?.detailsSubmitted
           && stripeInfo?.payoutsEnabled), [stripeInfo]);

  if (!authentication) return (<Pending />);
  return (

    <Container className="pt-4">

      <strong className="text-muted ">All about the money</strong>

      <Row className="mt-5">
        <Col xs={10}>
          <Row>

            <Col className="pb-0" xs={12}><strong className="bold ">Monetization</strong></Col>
            <Col xs={12}>
              <p className="bodyText ">
                Is your creation paid?
              </p>
            </Col>
          </Row>
        </Col>

        <Col xs={1} className="pl-2">
          <Form>
            <Form.Check
              ref={checkRef}
              defaultChecked={post?.isPaid}
              disabled={!isCreator()}
              type="switch"
              id="custom-switch"
              label=""
              onChange={() => updateButton()}
            />

          </Form>
        </Col>
      </Row>

      <Row className="mt-5 mb-2 pr-4">
        <Col xs={10}>
          <Row>

            <Col className="pb-0" xs={12}>
              <strong className="bold ">
                Price to unlock

              </strong>
            </Col>
            <Col xs={12}>
              <p className="bodyText ">
                Define a price to unlock this post for your followers
              </p>
            </Col>
          </Row>
        </Col>

        <Col xs={1} className="pl-0">
          {/* eslint-disable-next-line jsx-a11y/label-has-associated-control */}

          <CurrencyInput
            ref={priceRef}
            maxLength={6}
            className="mt-3 text-center "
            id="input-example"
            name="input-name"
            placeholder="5.99"
            prefix="$"
            disabled={!isCreator()}
            style={{
              borderColor: 'grey', borderRadius: '4px', borderStyle: 'solid', borderWidth: '0.3px', width: '68px',
            }}
            defaultValue={(post?.price || 0) / 100}
            decimalsLimit={2}
            onValueChange={() => updateButton()}
          />

        </Col>
      </Row>

      {!isCreator()
               && (
               <small>
                 In order to edit the pricing
                 information, please apply to becoming a creator

               </small>
               )}

      <Button
        className="py-3 px-3 w-100"
        style={{ marginTop: '200px' }}
        variant="primary"
        disabled={!enableSubmit}
        onClick={updatePost}
      >
        Publish post

      </Button>

      <div style={{ height: '100px' }} />

    </Container>
  );
});

export default Publish;
