import {
  CategoryAttributes, GenreAttributes,
} from '@audiochills/api-social/src/attributes';
import { ServiceAddons } from '@feathersjs/feathers';
import { observer } from 'mobx-react';
import { useLocalObservable } from 'mobx-react-lite';
import { fromPromise } from 'mobx-utils';
import React, { useCallback, useState } from 'react';
import {
  Button, Col, Container, Dropdown, DropdownButton, Row,
} from 'react-bootstrap';
import { generatePath, useHistory } from 'react-router-dom';
import { social } from '../../../../apis/social';
import fromServiceStream from '../../fromServiceStream';

export type CategoryGenreProps = {
  postId: string
};

export const CategoryGenre: React.FC<CategoryGenreProps> = observer(({ postId }) => {
  const [enableSubmit, setEnableSubmit] = useState(true);

  const [categoryChoice, setCategoryChoice] = useState<string | undefined | null>(undefined);
  const [genreChoice, setGenreChoice] = useState<string | undefined | null>(undefined);
  const [genreTitle, setGenreTitle] = useState<string | undefined | null>(undefined);
  const [categoryTitle, setCategoryTitle] = useState<string | undefined | null>(undefined);

  const history = useHistory();

  const store = useLocalObservable(() => ({

    get categoryList() {
      return fromServiceStream(() => (social.service('category') as ServiceAddons<CategoryAttributes>).watch().find({ where: {} }));
    },

    get genreList() {
      return fromPromise((social.service('genre') as any).find({ where: {} }) as Promise<GenreAttributes[]>);
    },

    get post() {
      return fromPromise(social.service('post').get(postId));
    },
  }));

  const navigateTo = useCallback((pathName: string) => {
    history.push({
      pathname: generatePath(`${pathName}/:postId`, { postId }),
    });
  }, []);

  const updateCategoryGenre = async () => {
    setEnableSubmit(false);

    try {
      await (social.service('post') as any).patch(postId, { genreId: genreChoice });

      navigateTo('/post/category-info');
    } catch (error) {
      setEnableSubmit(true);

      // eslint-disable-next-line no-alert
      alert('Error: please reload the page');
    }
  };

  return (

    <Container className="pt-4  pb-4" style={{ height: '75vh' }}>

      <strong className="bold ">Pick a category</strong>
      <br />
      <small className="smallText bodyText">Your category will define which genres you can choose</small>
      <br />
      {

          store.post.case({

            fulfilled: ({ genre }) => (

              <>
                {/* eslint-disable-next-line max-len */}
                {genre?.category?.id && !categoryChoice && setCategoryChoice(genre?.category?.id) && setCategoryTitle(genre?.category?.name)}

                <Row>

                  <Col xs={12}>

                    <DropdownButton
                      className="mt-3 mb-5"
                      menuAlign="right"
                      title={categoryTitle?.replaceAll('_', ' ') || 'Select a category'}
                      id="dropdown-menu-category"
                      variant="outline-primary"
                    >

                      {
                  store.categoryList.current.case({
                    fulfilled: ({ data }) => (
                      data?.map((categoryObj, idx) => (
                        <Dropdown.Item
                          className="handStyle"
                          key={categoryObj.id}
                          eventKey={idx.toString()}
                          onClick={() => {
                            setCategoryChoice(categoryObj.id);
                            setCategoryTitle(categoryObj.name);
                            setGenreChoice(undefined);
                            setGenreTitle(undefined);
                          }}
                        >
                          {categoryObj.name}
                        </Dropdown.Item>
                      ))

                    ),
                  })

                }
                    </DropdownButton>

                  </Col>

                </Row>

                <strong className="mt-5 bold ">Select your genre</strong>
                <br />
                <small className="smallText bodyText">You can only choose genres based on the category above</small>
                <br />

                {genre?.id && !genreChoice
                && setGenreChoice(genre?.id) && setGenreTitle(genre?.name)}
                <DropdownButton
                  menuAlign="right"
                  title={genreTitle?.replaceAll('_', ' ') || 'Select a genre'}
                  id="dropdown-menu-genre"
                  variant="outline-primary"
                  className="mt-3 mb-5"
                // eslint-disable-next-line no-console
                  onChange={(e) => console.log('change, ', e)}
                >
                  {
                  store.genreList.case({
                    // eslint-disable-next-line max-len
                    fulfilled: (obj) => (obj as any as GenreAttributes[])?.map((genreObj, idx) => (((genreObj as any).categoryId === categoryChoice)
                      ? (
                        <Dropdown.Item
                          className="handStyle"
                          key={genreObj.id}
                          eventKey={idx.toString()}
                          onClick={() => {
                            setGenreChoice(genreObj.id);
                            setGenreTitle(genreObj.name);
                          }}
                        >
                          {genreObj.name}

                        </Dropdown.Item>
                      )
                      : <></>)),
                  })

                }

                </DropdownButton>
              </>

            ),

          })

      }

      <Button
        className="py-3 px-3 w-100"
        style={{ marginTop: '150px' }}
        variant="primary"
        disabled={!enableSubmit || !genreChoice || !categoryChoice}
        onClick={() => updateCategoryGenre()}
      >
        Save information

      </Button>

      <div style={{ height: '72px' }} />

    </Container>
  );
});

export default CategoryGenre;
