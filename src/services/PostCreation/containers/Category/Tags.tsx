/* eslint-disable react/destructuring-assignment */
import { observer, useLocalObservable } from 'mobx-react';
import { fromPromise } from 'mobx-utils';
import React, { useCallback, useState } from 'react';
import { Button, Container } from 'react-bootstrap';
import { generatePath, Redirect, useHistory } from 'react-router-dom';
import { WithContext as ReactTags } from 'react-tag-input';
import { social, Pending } from '../../../../apis/social';
import './TagsPackageTheme.css';

export type TagsProps = {
  postId: string
};

const KeyCodes = {
  colon: 190,
  comma: 188,
  enter: 13,
  space: 32,
};

type ReactTagsType = {
  id: string,
  text: string
};

const delimiters = [KeyCodes.comma, KeyCodes.enter, KeyCodes.space, KeyCodes.colon];

export const Tags: React.FC<TagsProps> = observer(({ postId }) => {
  const [enableSubmit, setEnableSubmit] = useState(true);
  const history = useHistory();

  const [newTags, setNewTags] = useState<ReactTagsType[]>([]);

  const store = useLocalObservable(() => ({
    get post() {
      return fromPromise(social.service('post').get(postId));
    },
  }));

  const navigateTo = useCallback((pathName: string) => {
    history.push({
      pathname: generatePath(`${pathName}/:postId`, { postId }),
    });
  }, []);

  const updatePost = () => {
    setEnableSubmit(false);
    social.service('post').patch(postId, { customTags: newTags.map((t) => (t.id)) })
      .then(() => navigateTo('/post/category-info'))
      .catch(() => {
        setEnableSubmit(true);
      });
  };

  const processDbTags = (tags? : string[]) => {
    if (newTags.length !== 0) { return; }
    if (!tags) { return; }
    const modifTags: ReactTagsType[] = tags.map((t) => ({ id: t, text: t }));

    if (newTags.length === 0 && modifTags.length === 0) { return; }

    setNewTags(modifTags);
  };

  const handleDelete = (i: any) => setNewTags(newTags.filter((tag, index) => index !== i));

  // eslint-disable-next-line max-len
  const handleAddition = (tag: ReactTagsType) => newTags.length <= 8 && setNewTags([...newTags, tag]);

  const handleDrag = (tag: ReactTagsType, currPos: number, newPos: number) => {
    const tags = [...newTags];
    const draggedTags = tags.slice();
    draggedTags.splice(currPos, 1);
    draggedTags.splice(newPos, 0, tag);
    // re-render
    setNewTags(draggedTags);
  };
  return (

    <Container className="pt-4  pb-4" style={{ height: '75vh' }}>

      <strong className="bold ">Define your tags</strong>

      <br />

      <small className="smallText bodyText">Separate your tags by pressing space</small>

      <br />

      {
        store.post?.case({
          fulfilled: ({ customTags }) => (
            <div className="mt-3 w-100">
              {/* <textarea
                className="mt-3 w-100 p-3 border"
                style={{ height: '162px' }}
                placeholder="VAMPIRE FANTASY WHITCH"
                defaultValue={customTags || ''}
                onChange={(e) => setcustomDescription(e.currentTarget.value)}
              /> */}
              {processDbTags(customTags)}
              <ReactTags
                tags={newTags}
                inputFieldPosition="inline"
                placeholder="FANTASY"
                handleDelete={handleDelete}
                handleAddition={handleAddition}
                handleDrag={handleDrag}
                delimiters={delimiters}
              />
            </div>
          ),
          pending: () => <Pending />,
          rejected: () => (
            <Redirect to="/post/general/" />
          ),
        })
      }

      <Button
        className="py-3 px-3 w-100"
        style={{ marginTop: '300px' }}
        variant="primary"
        disabled={!enableSubmit || (newTags.length === 0)}
        onClick={() => updatePost()}
      >
        Save information

      </Button>

    </Container>
  );
});

export default Tags;
