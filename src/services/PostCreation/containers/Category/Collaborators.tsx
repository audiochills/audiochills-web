import { observer } from 'mobx-react';
import { useLocalObservable } from 'mobx-react-lite';
import { fromPromise } from 'mobx-utils';
import React, { useCallback, useState, useRef } from 'react';

import { Button, Container } from 'react-bootstrap';
import { generatePath, Redirect, useHistory } from 'react-router-dom';
import { social, Pending } from '../../../../apis/social';

export type CollaboratorsProps = {
  postId: string
};

export const Collaborators: React.FC<CollaboratorsProps> = observer(({ postId }) => {
  const [enableSubmit, setEnableSubmit] = useState(true);
  const [customDescription, setcustomDescription] = useState<string>('');
  const inpFieldRef = useRef<HTMLInputElement | null>(null);
  const history = useHistory();

  const store = useLocalObservable(() => ({
    get post() {
      return fromPromise(social.service('post').get(postId));
    },
  }));

  const navigateTo = useCallback((pathName: string) => {
    history.push({
      pathname: generatePath(`${pathName}/:postId`, { postId }),
    });
  }, []);

  const updatePost = useCallback((writerUsername: string) => {
    setEnableSubmit(false);
    social.service('post').patch(postId, { writerUsername })
      .then(() => navigateTo('/post/category-info'))
      .catch(() => setEnableSubmit(true));
  }, []);

  return (

    <Container className="pt-4 pb-4" style={{ height: '75vh' }}>

      {
        store.post.case({
          fulfilled: ({ writerUsername }) => (
            <>
              <strong className="bold ">Name your writer</strong>

              <br />

              <small className="smallText bodyText">The name of the song / script writer will be shown on the post</small>

              <br />

              <input
                ref={inpFieldRef}
                className="mt-3 py-3 px-3 w-100  border rounded"
                placeholder="Name of the writer"
                defaultValue={writerUsername || ''}
                onChange={(e) => setcustomDescription(e.currentTarget.value)}
              />

            </>
          ),
          pending: () => <Pending />,
          rejected: () => (
            <Redirect to="/post/general/" />
          ),
        })
      }
      <Button
        className="py-3 px-3 w-100"
        style={{ marginTop: '300px' }}
        variant="primary"
        disabled={!enableSubmit || (inpFieldRef?.current?.value.trim() === '') || inpFieldRef?.current?.value === undefined}
        onClick={() => updatePost(customDescription)}
      >
        Save information

      </Button>
    </Container>
  );
});

export default Collaborators;
