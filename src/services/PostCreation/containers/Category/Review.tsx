import { observer } from 'mobx-react';
import { useLocalObservable } from 'mobx-react-lite';
import { fromPromise } from 'mobx-utils';
import React, {
  useCallback, useRef, useState,
} from 'react';
import {
  Container, Row, Col, Image, Button, Form,
} from 'react-bootstrap';
import { generatePath, Redirect, useHistory } from 'react-router-dom';

import { social, Pending } from '../../../../apis/social';
import RightArrow from '../../images/rightArrow.svg';

export type CategoryInfoProps = {
  postId: string
};

export const Review: React.FC<CategoryInfoProps> = observer(({ postId }) => {
  const [enableSubmit, setEnableSubmit] = useState(false);
  const history = useHistory();
  const checkRef = useRef<any>(undefined);

  const store = useLocalObservable(() => ({
    get post() {
      return fromPromise(social.service('post').get(postId));
    },
  }));

  const navigateTo = useCallback((pathName: string) => {
    history.push({
      pathname: generatePath(`${pathName}/:postId`, { postId }),
    });
  }, []);

  const updatePost = () => {
    setEnableSubmit(false);

    social.service('post').patch(postId, { isNsfw: checkRef?.current.checked })
      .then(() => {
        navigateTo('/post/publish');
      })
      .catch(() => setEnableSubmit(true));
  };

  return (

    <Container className="pt-4  pb-5">
      <p className="bold ">Category Information</p>
      {
        store.post.case({
          fulfilled: ({
            isNsfw, customTags, writerUsername, genre,
          }) => (
            <>
              <Row className="mt-4">
                <Col xs={10}>
                  <Row>

                    <Col className="pb-0" xs={12}><strong className="bold ">Filter</strong></Col>
                    <Col xs={12}>
                      <p className="bodyText ">
                        Is your creation NSFW?
                      </p>
                    </Col>
                  </Row>
                </Col>

                <Col xs={1} className="pl-2">

                  <Form>
                    <Form.Check
                      ref={checkRef}
                      defaultChecked={isNsfw}
                      type="switch"
                      id="custom-switch"
                      label=""
                      onChange={(e) => social.service('post').patch(postId, { isNsfw: e.currentTarget.checked })}
                    />

                  </Form>
                </Col>
              </Row>

              <Row
                className="mt-5 mb-2 handStyle"
                onClick={() => navigateTo('/post/category-genre')}
              >
                <Col xs={10}>
                  <Row>

                    <Col className="pb-0" xs={12}><strong className="bold ">Category</strong></Col>
                    <Col xs={12}>
                      <p className="bodyText ">
                        {genre?.name || 'Help listeners discover your creation'}
                      </p>
                    </Col>
                  </Row>
                </Col>

                <Col xs={1}>
                  <Image className="mt-3" src={RightArrow} style={{ height: '16px', width: '16px' }} />
                </Col>
              </Row>

              <Row
                className="mt-5 mb-2 handStyle"
                onClick={() => navigateTo('/post/category-genre')}
              >
                <Col xs={10}>
                  <Row>

                    <Col className="pb-0" xs={12}><strong className="bold ">Genre</strong></Col>
                    <Col xs={12}>
                      <p className="bodyText ">
                        {genre?.name || 'Help listeners discover your creation'}
                      </p>
                    </Col>
                  </Row>
                </Col>

                <Col xs={1}>
                  <Image className="mt-3" src={RightArrow} style={{ height: '16px', width: '16px' }} />
                </Col>
              </Row>

              <Row
                className="mt-5 handStyle"
                onClick={() => navigateTo('/post/tags')}
              >
                <Col xs={10}>
                  <Row>

                    <Col className="pb-0" xs={12}><strong className="bold ">User defined tags</strong></Col>
                    <Col xs={12}>
                      <p className="bodyText ">
                        { (<strong>{ customTags?.join(', ')}</strong>)
                          || 'Tag your creation the right way'}
                      </p>
                    </Col>
                  </Row>
                </Col>

                <Col xs={1}>
                  <Image className="mt-3" src={RightArrow} style={{ height: '16px', width: '16px' }} />
                </Col>
              </Row>

              <Row
                className="mt-5 handStyle"
                onClick={() => navigateTo('/post/collaborators')}
              >
                <Col xs={10}>
                  <Row>

                    <Col className="pb-0" xs={12}><strong className="bold ">Collaborators</strong></Col>
                    <Col xs={12}>
                      <p className="bodyText ">

                        {writerUsername || 'Please give credit to your collaborators'}
                      </p>
                    </Col>
                  </Row>
                </Col>

                <Col xs={1}>
                  <Image className="mt-3" src={RightArrow} style={{ height: '16px', width: '16px' }} />
                </Col>
              </Row>

              <Button
                className="mt-5 py-3 px-3 w-100"
                variant="primary"
                disabled={!enableSubmit && !genre?.id}
                onClick={() => updatePost()}
              >
                Continue to meta information

              </Button>

            </>
          ),
          pending: () => <Pending />,
          rejected: () => (
            <Redirect to="/post/general/" />
          ),
        })
      }

    </Container>
  );
});

export default Review;
