import { observer } from 'mobx-react';
import { useLocalObservable } from 'mobx-react-lite';
import { fromPromise } from 'mobx-utils';
import React, {
  useCallback, useState,
} from 'react';

import {
  Container, Row, Col, Image, Button,
} from 'react-bootstrap';
import {
  generatePath, Link, Redirect,
} from 'react-router-dom';

import { social, Pending } from '../../../../apis/social';
import RightArrow from '../../images/rightArrow.svg';

export type CategoryInfoProps = {
  postId: string
};

export const CategoryInfo: React.FC<CategoryInfoProps> = observer(({ postId }) => {
  const [enableSubmit, setEnableSubmit] = useState(false);

  const store = useLocalObservable(() => ({
    get post() {
      return fromPromise(social.service('post').get(postId));
    },
  }));

  const navigateTo = useCallback((pathName: string) => generatePath(`${pathName}/:postId`, { postId }), [postId]);

  const updatePost = async () => {
    setEnableSubmit(false);

    try {
      await social.service('post').patch(postId, { isNsfw: false }); // checkRef?.current?.checked
    } catch (e) {
      setEnableSubmit(true);
    }
  };

  return (
    <Container className="pt-4">
      <p className="bold ">Category Information</p>
      {
        store.post.case({
          fulfilled: ({
            customTags, writerUsername, genre,
          }) => (
            <>
              <Link to={navigateTo('/post/category-genre')}>
                <Row
                  className="mt-4 mb-3 handStyle"
                >
                  <Col xs={10}>
                    <Row>

                      <Col className="pb-0" xs={12}>
                        <strong className="bold">
                          {genre?.category?.name ? 'Category' : 'Category / Genre'}
                        </strong>
                      </Col>

                      <Col xs={12}>
                        <p className="mb-0 bodyText ">
                          {genre?.category?.name
                            ? (<strong className="bold">{genre?.category?.name}</strong>)
                            : ('Help listeners discover your creation')}
                        </p>
                      </Col>

                    </Row>
                  </Col>

                  <Col xs={1}>
                    <Image className="mt-3" src={RightArrow} style={{ height: '16px', width: '16px' }} />
                  </Col>
                </Row>
              </Link>

              {
               genre?.id && (
                 <Link to={navigateTo('/post/category-genre')}>

                   <Row
                     className="mt-4 mb-3 handStyle"
                   >
                     <Col xs={10}>
                       <Row>

                         <Col className="pb-0" xs={12}>

                           <strong className="bold">
                             Genre
                           </strong>

                         </Col>
                         <Col xs={12}>
                           <strong className="bold">
                             {genre?.name}
                           </strong>
                         </Col>
                       </Row>
                     </Col>

                     <Col xs={1}>
                       <Image className="mt-3" src={RightArrow} style={{ height: '16px', width: '16px' }} />
                     </Col>
                   </Row>
                 </Link>

               )
              }

              <Link to={navigateTo('/post/tags')}>

                <Row
                  className="mt-4 mb-3 handStyle"
                >
                  <Col xs={10}>
                    <Row>

                      <Col className="pb-0" xs={12}>
                        <strong className="bold">
                          User defined tags
                        </strong>
                      </Col>

                      <Col xs={12}>
                        <p className="mb-0 bodyText ">
                          {customTags
                            ? (<strong>{ customTags.join(', ')}</strong>)
                            : ('Tag your creation the right way')}
                        </p>
                      </Col>

                    </Row>
                  </Col>

                  <Col xs={1}>
                    <Image className="mt-3" src={RightArrow} style={{ height: '16px', width: '16px' }} />
                  </Col>
                </Row>

              </Link>

              <Link to={navigateTo('/post/collaborators')}>
                <Row
                  className="mt-4 mb-3 handStyle"
                >
                  <Col xs={10}>
                    <Row>

                      <Col className="pb-0" xs={12}>
                        <strong className="bold">
                          Collaborators
                        </strong>
                      </Col>

                      <Col xs={12}>

                        <p className="mb-0 bodyText ">
                          {writerUsername
                            ? (<strong>{ writerUsername}</strong>)
                            : ('Please give credit to your collaborators')}
                        </p>
                      </Col>
                    </Row>
                  </Col>

                  <Col xs={1}>
                    <Image className="mt-3" src={RightArrow} style={{ height: '16px', width: '16px' }} />
                  </Col>
                </Row>

              </Link>

              <Link to={navigateTo('/post/publish')}>
                <Button
                  className="mt-5 py-3 px-3 w-100"
                  variant="primary"
                  disabled={!enableSubmit && !genre?.id}
                  onClick={() => updatePost()}
                >
                  Continue to meta information

                </Button>
              </Link>

            </>
          ),
          pending: () => <Pending />,
          rejected: () => (
            <Redirect to="/post/general/" />
          ),
        })
      }

    </Container>
  );
});

export default CategoryInfo;
