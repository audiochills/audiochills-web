import React from 'react';
import { Col, Container, Row } from 'react-bootstrap';

export const Upload: React.FC<{ progress: number }> = ({ progress }) => (

  <div
    className="d-flex justify-content-center align-items-center"
    style={{
      backgroundColor: 'rgba(0,0,0,.7)',
      bottom: '0',
      left: '0',
      position: 'fixed',
      right: '0',
      top: '0',
      zIndex: 9999,
    }}
  >
    <Container style={{ marginTop: '-300px' }}>

      <Row>

        <Col xs={12}>
          <h4 className="text-center text-primary">
            {progress}
            %
          </h4>
        </Col>
        <Col xs={12}>
          <p className="text-center text-warning" style={{ width: '100%' }}>Uploading thumbnail/audio...</p>
        </Col>
      </Row>

    </Container>

  </div>

);

export default Upload;
