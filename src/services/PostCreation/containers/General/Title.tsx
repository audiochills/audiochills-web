/* eslint-disable react/destructuring-assignment */
import { PostAttributes } from '@audiochills/api-social/src/attributes';
import { observer } from 'mobx-react';
import { useLocalObservable } from 'mobx-react-lite';
import { fromPromise } from 'mobx-utils';
import React, { useCallback, useState } from 'react';
import { Button, Container } from 'react-bootstrap';
import { generatePath, Redirect, useHistory } from 'react-router-dom';
import { social, Pending } from '../../../../apis/social';

export type TitleProps = {
  postId: string
  post?: PostAttributes
};

export const Title: React.FC<TitleProps> = observer(({ postId }) => {
  const [enableSubmit, setEnableSubmit] = useState(true);
  const [customTitle, setcustomTitle] = useState<string>('');
  const history = useHistory();

  const store = useLocalObservable(() => ({
    get post() {
      return fromPromise(social.service('post').get(postId));
    },
  }));

  const navigateTo = useCallback((pathName: string) => {
    history.push({
      pathname: generatePath(`${pathName}/:postId`, { postId }),
    });
  }, []);

  const updatePost = useCallback((title: string) => {
    setEnableSubmit(false);
    social.service('post').patch(postId, { title })
      .then(() => navigateTo('/post/general'))
      .catch(() => setEnableSubmit(true));
  }, []);

  return (

    <Container className="pt-4 pb-4">

      <strong className="creatorTitle ">Title</strong>

      <br />

      {
        store.post.case({
          fulfilled: ({ title }) => (
            <>
              <textarea
                className="mt-3 w-100 p-3 border rounded"
                style={{ height: '162px' }}
                placeholder="Give your creation a precise title."
                defaultValue={title || ''}
                onChange={(e) => setcustomTitle(e.currentTarget.value)}
              />
            </>

          ),
          pending: () => <Pending />,
          rejected: () => (
            <Redirect to="/post/general/" />
          ),
        })
      }
      <Button
        className="py-3 px-3 w-100"
        style={{ marginTop: '150px' }}
        variant="primary"
        disabled={!enableSubmit || (customTitle === '')}
        onClick={() => updatePost(customTitle)}
      >
        Save information

      </Button>
    </Container>
  );
});

export default Title;
