/* eslint-disable react/destructuring-assignment */
import React, { useCallback, useState } from 'react';
import {
  Button, Col, Container, Image, Row,
} from 'react-bootstrap';

import { observer } from 'mobx-react';
import { uuid } from 'uuidv4';
import { generatePath, useHistory } from 'react-router-dom';
import ImageUploading, { ImageListType } from 'react-images-uploading';
import axios from 'axios';
import { useDropzone } from 'react-dropzone';
import { useObservable, useRetryableObservable } from 'react-use-observable';
import { of } from 'rxjs';
import UploadImg from '../../images/upload.svg';
import RightArrow from '../../images/rightArrow.svg';
import Camera from '../../images/camera.svg';
import AvatarPlaceHolder from '../../images/avatar-placeholder.png';
import { Pending, social } from '../../../../apis/social';
import { Upload } from './Upload';
import './GeneralInfo.css';

export type GeneralInfoProps = {
  postId?: string
};

function dataURItoBlob(dataURI:string, mimeType: string) {
  const binary = atob(dataURI.split(',')[1]);
  const array = [];
  // eslint-disable-next-line no-plusplus
  for (let i = 0; i < binary.length; i++) {
    array.push(binary.charCodeAt(i));
  }
  return new Blob([new Uint8Array(array)], { type: mimeType });
}

export const GeneralInfo: React.FC<GeneralInfoProps> = observer(({ postId }) => {
  const [isUploading, setIsUploading] = useState(false);
  const [progress, setProgress] = useState<number>(0);
  const [postImg, setPostImg] = useState<string | undefined>();
  const [newPostId] = useState<string>(postId || uuid());
  const history = useHistory();

  const [authentication] = useObservable(() => social.authentication.result(), []);

  const [postAudio, , , retryPostAudio] = useRetryableObservable(
    () => (!authentication ? of(null) : social.service('post-audio').watch().get(`postId:${newPostId}`)),
    [authentication, newPostId],
  );
  const [postImage, , , retryPostImage] = useRetryableObservable(
    () => (!authentication ? of(null) : social.service('post-image').watch().get(`postId:${newPostId}`)),
    [authentication, newPostId],
  );

  const [post] = useObservable(
    () => (!authentication ? of(null) : social.service('post').watch().get(newPostId)),
    [authentication, newPostId],
  );

  const navigateTo = (pathName: string) => {
    history.push({
      pathname: generatePath(`${pathName}/:postId`, { postId: newPostId }),
    });
  };

  // useEffect(() => {
  //   if (postId) { return; }
  //   (social.service('post') as any).create({
  //     id: newPostId,
  //     postAudio: {
  //       postId: newPostId,
  //     },
  //     postImage: {
  //       postId: newPostId,
  //     },
  //   })
  //     .then(() => navigateTo('/post/general'))
  //   // eslint-disable-next-line no-alert
  //     .catch((e:any) => alert(`Error: please reload your browser ${e}`));
  // }, [postId]);

  // const validateMedia = () => (postAudio as any)?.exists && (postImage as any)?.exists;

  const validateMedia = () => (postAudio as any)?.exists;

  const uploadImage = async (dataUrl: string | undefined) => {
    if (!dataUrl) { return; }

    setIsUploading(true);
    const mimeType = dataUrl.substring(dataUrl.indexOf(':') + 1, dataUrl.indexOf(';')); // => image/png
    const extension = mimeType.replace('image/', '');
    const blob = dataURItoBlob(dataUrl, mimeType);
    const reader = new FileReader();

    const { uploadUrl }: { uploadUrl: string } = await (social.service('post-image') as any)
      .patch(postImage?.id ?? '_', { extension, mimeType });

    reader.onload = (e) => {
      axios.request({
        data: e.target?.result,
        headers: { 'Content-Type': mimeType },
        method: 'PUT',
        onUploadProgress: (p) => {
          const percentage = parseFloat(((p.loaded / p.total) * 100).toFixed(1));
          setProgress(percentage);
        },
        url: uploadUrl,
      }).then(() => {
        setIsUploading(false);
        setProgress(0);
        retryPostImage();
      }).catch(() => setIsUploading(false));
    };
    reader.readAsArrayBuffer(blob);
  };

  const uploadAudio = async (dataUrl: File) => {
    if (!dataUrl) { return; }
    if (!postAudio) return;

    setIsUploading(true);

    const mimeType = dataUrl.type;
    const extension = mimeType.split('/').pop();

    if (extension?.toLowerCase() !== 'mp3' && extension?.toLowerCase() !== 'wav'
    && extension?.toLowerCase() !== 'wave' && extension?.toLowerCase() !== 'aac'
    && extension?.toLowerCase() !== 'flac' && extension?.toLowerCase() !== 'mpeg') {
      // eslint-disable-next-line no-alert
      alert('Incorrect audio format');
    }

    const { uploadUrl }: { uploadUrl: string } = await (social.service('post-audio') as any)
      .patch(postAudio.id, { extension, mimeType });

    axios.request({
      data: dataUrl,
      headers: { 'Content-Type': mimeType },
      method: 'PUT',
      onUploadProgress: (p) => {
        const percentage = parseFloat(((p.loaded / p.total) * 100).toFixed(1));
        setProgress(percentage);
      },
      url: uploadUrl,
    }).then(() => {
      setIsUploading(false);
      setProgress(0);
      retryPostAudio();
    }).catch(() => {
      setIsUploading(false);
    });
  };

  const onChange = (imageList: ImageListType) => {
    setPostImg(imageList[0]?.dataURL);
    uploadImage(imageList[0]?.dataURL);
  };

  const onDrop = useCallback((acceptedFiles) => {
    // Do something with the files
    uploadAudio(acceptedFiles[0]);
  }, [postAudio]);

  const {
    getRootProps, getInputProps, inputRef,
  } = useDropzone({ accept: '.mp3, .wav, .wave, .m4a, .flac, .aac', onDrop });

  if (!authentication || !post) return <Pending />;

  return (

    <Container className="pb-4">

      {isUploading && <Upload progress={progress} />}

      <br />
      <strong className="mb-1 mt-4 mb-3 bold text-muted">General Information</strong>
      <br />
      <br />

      <strong className="bold ">Post Visual</strong>
      {' '}
      <br />

      <ImageUploading
        onChange={onChange}
        value={[postImg] as any as ImageListType}
        maxFileSize={2e+7}
      >
        {({
          onImageUpload,
        }) => (
          <div
            className="d-flex justify-content-center align-items-center"
            style={{ height: '188px', width: '188px' }}
          >
            <Image
              className="cursorPointer"
              src={Camera}
              style={{ height: '36px', position: 'fixed', width: '36px' }}
              onClick={onImageUpload}
            />
            <Image
              className="mt-2 cursorPointer"
              src={(postImage as any)?.exists ? (postImage as any)?.downloadUrl : AvatarPlaceHolder}
              style={{
                borderRadius: '22px', height: '100%', objectFit: 'cover', overflow: 'hidden', width: '100%',
              }}
              onClick={onImageUpload}

            />
          </div>
        )}
      </ImageUploading>

      {/* <Image className="mt-2"
      rounded src={url} style={{ height: '188px', objectFit: 'cover', width: '188px' }} /> */}

      {/* eslint-disable-next-line react/jsx-props-no-spreading */}
      <div style={{ display: 'none' }} {...getRootProps()}>
        {/* eslint-disable-next-line react/jsx-props-no-spreading */}
        <input {...getInputProps()} />
      </div>

      <Row
        className="mt-5 mb-4 cursorPointer"
        onClick={() => {
          inputRef.current?.click();
        }}
      >
        <Col xs={10}>
          <Row>
            <Col className="pb-0 " xs={12}><strong className="bold ">Upload file</strong></Col>
            <Col xs={12}>
              <p className="bodyText ">
                {(postAudio as any)?.exists
                  ? 'Uploaded'
                  : 'Formats include mp3, wav & ogg'}
              </p>
            </Col>
          </Row>
        </Col>

        <Col xs={1}>
          <Image className="mt-4" src={UploadImg} style={{ height: '24px', width: '24px' }} />
        </Col>
      </Row>

      <Row
        className="mt-2 mb-4 cursorPointer"
        onClick={() => navigateTo('/post/title')}
      >
        <Col xs={10}>
          <Row>
            <Col className="pb-0" xs={12}><strong className="bold ">Title</strong></Col>
            <Col xs={12}>
              <p className="bodyText ">
                {post?.title || 'Name your creation'}
              </p>
              {/* </LinkContainer> */}
            </Col>
          </Row>
        </Col>

        <Col xs={1}>
          <Image className="mt-4" src={RightArrow} style={{ height: '16px', width: '16px' }} />
        </Col>
      </Row>

      <Row
        className="mt-2 mb-4 cursorPointer"
        onClick={() => navigateTo('/post/description')}
      >
        <Col xs={10}>
          <Row>
            <Col className="pb-0" xs={12}><strong className="bold ">Description</strong></Col>
            <Col xs={12}>
              <p className="bodyText ">
                {post?.description || 'Describe your creation'}
              </p>
              {/* </LinkContainer> */}
            </Col>
          </Row>
        </Col>

        <Col xs={1}>
          <Image className="mt-4" src={RightArrow} style={{ height: '16px', width: '16px' }} />
        </Col>
      </Row>

      <Button
        className="mt-4 py-3 px-3 w-100 buttonText"
        variant="primary"
                    // eslint-disable-next-line max-len
        disabled={!post?.description || !validateMedia()}
        onClick={() => navigateTo('/post/category-info')}
      >
        Continue to category information

      </Button>

      <p style={{ marginBottom: '100px' }} />
    </Container>

  );
});

export default GeneralInfo;
