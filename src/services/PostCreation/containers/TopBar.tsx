import React from 'react';
import {
  Col, Image, ProgressBar, Row,
} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import LeftArrow from '../images/leftArrow.svg';

export type TopBarAttributes = {
  navigateBackTo: string
  postId?: string
  title: string
  progress: number
};
export const TopBar: React.FC<TopBarAttributes> = (
  {
    navigateBackTo, postId, children, title, progress,
  },
) => {
  const navigateTo = () => {
    if (postId) return `${navigateBackTo}/${postId}`;
    return navigateBackTo;
  };
  return (
    <>

      <div
        className="d-flex justify-content-center"
        style={{
          left: 0, position: 'fixed', right: 0, top: 0, zIndex: 1000,
        }}
      >

        <Row
          className="d-flex justify-content-start bg-light "
          style={{ maxWidth: '600px', width: '100%' }}
        >

          <Col
            xs={1}
            className="d-flex justify-content-end align-items-center pl-4"
          >
            <Link to={navigateTo()}>
              <Image
                className=""
                src={LeftArrow}
                style={{ height: '22px', width: '22px' }}
              />
            </Link>

          </Col>

          <Col className="pl-0 d-flex align-items-center ">
            <p className="mt-3 bodyText colorTextTitles">
              {title}
            </p>
          </Col>
        </Row>

        <ProgressBar now={progress} style={{ height: '2px' }} />
      </div>

      {children}

    </>
  );
};

export default TopBar;
