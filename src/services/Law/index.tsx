import React from 'react';
import { Route } from 'react-router-dom';
import CopyRightAndLicenses from './containers/CopyrightAndLicenses';
import PrivacyPolicy from './containers/PrivacyPolicy';
import TermsAndAgreement from './containers/TermsAndAgreement';

export default () => [
  <Route exact path="/privacy-policy" render={() => <PrivacyPolicy />} />,
  <Route exact path="/terms-of-service" render={() => <TermsAndAgreement />} />,
  <Route exact path="/copyright-licenses" render={() => <CopyRightAndLicenses />} />,
];
