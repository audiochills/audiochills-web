import React from 'react';
import { Container } from 'react-bootstrap';

export const PrivacyPolicy: React.FC<{}> = () => (
  <Container>

    <iframe
      title="CopyrightAndLicenses"
      style={{ height: '100vh', width: '100%' }}
      src="https://www.iubenda.com/privacy-policy/40961075"
    />

  </Container>
);

export default PrivacyPolicy;
