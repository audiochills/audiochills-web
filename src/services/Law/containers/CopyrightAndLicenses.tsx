import React from 'react';
import { Container } from 'react-bootstrap';

export const CopyRightAndLicenses: React.FC<{}> = () => (
  <Container>

    <iframe
      title="CopyrightAndLicenses"
      style={{ height: '100vh', width: '100%' }}
      src="https://docs.google.com/document/d/e/2PACX-1vS_ko0fVdpjJZFdxT5FcOjhXO2HUcGZKnqHXYZvwWXNmWS7D51m3pnn_qlOCyDVk5CW6kyWdTc_PXSz/pub"
    />

  </Container>
);

export default CopyRightAndLicenses;
