import React, {
  useRef, useState, useCallback, useEffect,
} from 'react';
import {
  Button,
  Col,
  Container,
  Image,
  Row,
} from 'react-bootstrap';
import ReactPlayer from 'react-player';
import { useModalManager } from '@vlzh/react-modal-manager';
import { PostAttributes } from '@audiochills/api-social/src/attributes';
import { useObservable } from 'react-use-observable';
import AvatarPlaceHolder from '../images/avatar-placeholder.png';
import Pause from '../images/pause.svg';
import Play from '../images/play.svg';
// eslint-disable-next-line import/no-cycle
import PlayerFullScreen, { PlayerFullScreenProps } from './PlayerFullScreen';
import social from '../../../apis/social';

const audioBarStyle = {
  background: 'linear-gradient(180deg, #F6687B -138.71%, #FFBF73 118.55%)',
  borderRadius: '10px 10px 0 0',
  marginBottom: '64px',
};

export type BottomPlayerModalParams = null | {
  post: PostAttributes,
  forcePlay: boolean
};

export const BottomPlayer: React.FC = () => {
  const [playerBarHidden, setPlayerBarHidden] = useState(false);
  const [isPlaying, setIsPlaying] = useState(false);
  const [seekTime, setSeekTime] = useState(0);
  const [playedSeconds, setPlayedSeconds] = useState<number>(0);
  const playerRef = useRef<ReactPlayer>(null);
  const seekBarRef = useRef<HTMLInputElement>(null);

  const {
    isOpen, getParams,
  } = useModalManager('BottomPlayerModal');

  const params = getParams() as BottomPlayerModalParams;
  const { post, forcePlay } = params || { forcePlay: false, post: undefined };

  useEffect(() => setIsPlaying(forcePlay), [forcePlay]);

  const [postAudio] = useObservable(
    () => (social.service('post-audio').watch().get(`postId:${post?.id}`)),
    [post?.id],
  );

  const [postImage] = useObservable(
    () => (social.service('post-image').watch().get(`postId:${post?.id}`)),
    [post?.id],
  );

  const playerFullScreenProps = useCallback(() : PlayerFullScreenProps => ({
    isPlaying,
    playedSeconds,
    playerRef,
    post,
    seekBarRef,
    seekTime,
    setIsPlaying,
    setPlayerBarHidden,
    setSeekTime,
    thumbnail: postImage?.exists ? postImage?.downloadUrl : AvatarPlaceHolder,
  }), [isPlaying, playedSeconds, playerRef, seekBarRef, seekTime, postImage, postAudio, post?.id]);

  // console.log('BottomPlayerModal ', postAudio);

  return (
    <>

      { playerBarHidden
        // eslint-disable-next-line react/jsx-props-no-spreading
        && (<PlayerFullScreen {...playerFullScreenProps()} />)}
      <ReactPlayer
        ref={playerRef}
        width="0"
        height="0"
        playsinline
        playing={isPlaying}
        controls={false}
        config={{
          file: {
            forceAudio: true,
          },
        }}
        url={(postAudio as any)?.downloadUrl}
        onPlay={() => setIsPlaying(true)}
        onPause={() => setIsPlaying(false)}
        onProgress={(e) => {
          setPlayedSeconds(e.playedSeconds);
          if (seekBarRef.current) {
            seekBarRef.current!.value = e.playedSeconds.toString();
          }
        }}
      />
      <Container
        className="fixed-bottom y-0 py-2 px-3"
        hidden={playerBarHidden || !isOpen()}
        style={audioBarStyle}
      >
        <Row className="pl-3" style={{ maxHeight: '62px' }}>
          <Col
            xs={2}
            md={1}
            className="d-flex align-items-center justify-content-center px-0"

          >
            <Button className="w-100 h-100" variant="link" onClick={() => setIsPlaying(!isPlaying)}>
              <Image
                className="p-0"
                style={{ height: '35px', width: '35px' }}
                src={isPlaying ? Pause : Play}
                alt="icon"
              />

            </Button>

          </Col>
          <Col
            xs={10}
            className="pt-2"
            onClick={() => setPlayerBarHidden(true)}
            style={{ cursor: 'pointer' }}
          >
            <Row className="justify-content-center">
              <Col className="d-flex justify-content-center">
                <h6 className="text-light text-wrap pr-5">
                  {
                  post
                  && ((post?.title?.length > 20)
                    ? (`${post?.title?.slice(0, 20)}...`)
                    : post?.title)
                  }
                </h6>

              </Col>

            </Row>
            <Row className="justify-content-center">
              <Col className="d-flex justify-content-center"><small className="text-light text-wrap pr-5">{post?.author?.username}</small></Col>

            </Row>
          </Col>
        </Row>
      </Container>

    </>
  );
};

export default BottomPlayer;
