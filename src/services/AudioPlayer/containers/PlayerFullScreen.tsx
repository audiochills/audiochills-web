import React, { useCallback, useState, useEffect } from 'react';
import {
  Col, Container, Image, Row, Badge, Button,
} from 'react-bootstrap';
import ReactPlayer from 'react-player';
import { PostAttributes } from '@audiochills/api-social/src/attributes';
import { LinkContainer } from 'react-router-bootstrap';
import { of } from 'rxjs';
import { useModalManager } from '@vlzh/react-modal-manager';
import { useObservable } from 'react-use-observable';
import Dropdown from '../images/dropdown.svg';
import Pause from '../images/pause.svg';
import Play from '../images/play.svg';
import social from '../../../apis/social';
import CommentSvg from '../images/reactionComment.svg';
import HeartSvgDisabled from '../images/reactionHeart.svg';
import HeartSvgActive from '../images/reactionHeartRed.svg';
import ShareSvg from '../images/reactionShare.svg';
import TipSvg from '../images/reactionTip.svg';
import WebShare from '../../Profile/containers/WebShare';
import keycloak from '../../../keycloak';

// eslint-disable-next-line import/no-cycle

const convert = (value: number): string => `${Math.floor(value / 60)}:${value % 60 ? value % 60 : '00'}`;

export type PlayerFullScreenProps = {
  thumbnail?: string
  isPlaying: boolean
  playerRef: React.RefObject<ReactPlayer>
  setSeekTime: Function
  seekTime: number
  setIsPlaying: Function
  setPlayerBarHidden: Function
  playedSeconds: number
  seekBarRef: React.RefObject<HTMLInputElement>
  post?: PostAttributes
};

const PlayerFullScreen: React.FC<PlayerFullScreenProps> = ({
  isPlaying, setIsPlaying, setPlayerBarHidden, thumbnail,
  playerRef, playedSeconds, seekBarRef, post,
}) => {
  const [likeCount, setLikeCount] = useState<number>(-1);

  const { openModal } = useModalManager();

  const [authentication] = useObservable(() => social.authentication.result(), []);
  const openTipModal = useCallback(() => {
    openModal('TipPostModal', false, { postId: post?.id, postOverride: post });
  }, [post]);

  const [profileLikePost] = useObservable(
    () => (!authentication
      ? of(authentication)
      : ((social.service('profile-like-post') as any).watch().find({ query: { postId: post?.id, profileId: authentication.profile.id } }))),
    [post, post?.authorId, authentication],
  );
  const [stripeInfo] = useObservable(
    () => social.service('stripe-info').watch().get(`profileId:${post?.authorId}`),
    [post?.authorId],
  );
  const manageLike = useCallback(async () => {
    if (!authentication) return keycloak && await keycloak.login();
    if (!(profileLikePost as any)?.data?.length) {
      await (social.service('profile-like-post') as any).create({ postId: post!.id });
      setLikeCount(likeCount + 1);
    } else {
      await (social.service('profile-like-post') as any).remove((profileLikePost as any)?.data[0]?.id);
      setLikeCount(likeCount - 1);
    }
    return undefined;
  }, [post, likeCount, profileLikePost, (profileLikePost as any)?.id]);

  useEffect(() => {
    setLikeCount((post as any)?.likeCount);
  }, [(post as any)?.likeCount, (post as any)?.id]);

  return (

    <div
      className="position-fixed d-flex"
      style={{
        bottom: 0, left: 0, right: 0, top: 0, zIndex: 2000,
      }}
    >

      <div
        className="position-fixed d-flex justify-content-center"
        style={{
          backgroundColor: 'transparent', bottom: 0, left: 0, objectFit: 'cover', right: 0, top: 0, zIndex: -1,
        }}
      >

        <Image
          className="h-100 w-100"
          style={{ maxWidth: '600px', objectFit: 'cover' }}
          alt=""
          src={thumbnail}
        />
      </div>

      <div
        className="position-fixed vh-100 vw-100"
        style={{ backgroundColor: 'transparent', opacity: 0.3, zIndex: 0 }}
      />

      <Container className="px-4 pt-4 w-100">

        <Row className="pl-2 d-flex justify-content-around">
          <Col className="p-0 d-flex align-items-center">
            <p className=" py-1 px-2 m-0 bg-dark text-light">{post?.author?.username}</p>
          </Col>

          <Col
            xs={2}
            sm={1}
            className="pr-3 d-flex align-items-center justify-content-end"
          >
            <Button
              variant="link"
              onClick={() => setPlayerBarHidden(false)}
            >

              <img
                className="w-20 p-0"
                style={{ objectFit: 'cover', zIndex: 2001 }}
                alt=""
                src={Dropdown}
              />
            </Button>

          </Col>
        </Row>

        <Row className="pl-2 pt-2">
          <h5 className=" py-1 px-2 text-wrap bg-dark text-light" style={{ zIndex: 1 }}>{post?.title}</h5>
        </Row>
        {/* <Row className="pl-2">
          <small className=" py-2 px-2 text-wrap bg-dark text-light" style={{ zIndex: 1 }}>
            Written by
            {' '}
            {post?.writerUsername}
          </small>
        </Row> */}

        <Row className="mt-4">

          {
            post?.customTags?.map((e) => (
              <Badge
                className="py-2 px-2 text-muted bg-light mr-2 ml-1"
                style={{ borderRadius: '5px', zIndex: 1 }}
              >
                {e}
              </Badge>
            ))

        }

        </Row>

      </Container>

      <Container className="fixed-bottom pb-3">
        <Row className="d-flex justify-content-between">
          <Col className="text-light" xs={2}><small>{convert(Math.floor(playedSeconds))}</small></Col>
          {/* <Col xs={8} /> */}
          <Col className="text-light d-flex justify-content-end" xs={2}><small>{convert(Math.floor(playerRef.current?.getDuration() ?? 0))}</small></Col>
        </Row>
        {/* <ProgressBar className=" mt-0 mb-2 bg-light" style={{ height: '3px' }} now={60} /> */}
        <input
          ref={seekBarRef}
          type="range"
          className="custom-range"
          min="0"
          style={{ color: 'white', zIndex: 1 }}
          defaultValue={playerRef.current?.getCurrentTime()}
          max={playerRef.current?.getDuration()}
          onTouchStart={() => {
            setIsPlaying(false);
          }}
          onTouchEnd={(e) => {
            playerRef.current?.seekTo(e.currentTarget.value as any as number, 'seconds');
            setIsPlaying(true);
          }}
        />

        <div className="d-flex justify-content-center mt-3">

          <Row className="justify-content-around">

            <Col xs={3} className="d-flex align-items-center">
              {/* <img alt="" src={LeftArrow} /> */}
            </Col>

            {/* //play button */}
            <Col xs={6} className="d-flex align-items-center">
              <Button
                variant="link"
                onClick={() => setIsPlaying(!isPlaying)}
              >
                <Image
                  style={{ height: '50px', width: '50px' }}
                  src={isPlaying ? Pause : Play}
                  alt="icon"
                />

              </Button>

            </Col>
            <Col xs={3} className="d-flex align-items-center">
              {/* <img alt="" src={RightArrow} /> */}

            </Col>
          </Row>

        </div>
        {/*
        <div className="d-flex justify-content-center mb-4">
          <Row className="mt-4">

            <Reaction src={HeartWhite} numb={post?.likeCount ?? 0} onClick={() => {}} />
            <Reaction src={Comment} numb={post?.commentCount ?? 0} onClick={() => {}} />
            <Reaction src={Share} onClick={() => {}} />
            <Reaction src={Tip} numb={`$${(post as any)?.tipTotalCents ?? 0}`} onClick={() => {}} />

          </Row>

        </div>
      */}
        <br />
        <div className="w-100 d-flex justify-content-center">
          <div
            className="d-flex justify-content-around mt-2 mb-4 w-100 px-4  text-light"
            style={{ maxWidth: '350px' }}
          >

            <Button
              variant="link"
              size="sm"
              className=" text-light"
              onClick={manageLike}
            >
              {/* <FontAwesomeIcon icon={faHeart}
              color={(profileLikePost as any)?.data?.length > 0 ? 'red' : 'grey'} /> */}

              <Image
                className="mb-1"
                src={(profileLikePost as any)?.data?.length > 0 ? HeartSvgActive : HeartSvgDisabled}
                width="20px"
                height="20px"
              />
              {' '}
              {likeCount}
            </Button>

            <LinkContainer
              to={`/post/${post?.id}`}
              onClick={() => setPlayerBarHidden(false)}
              exact
            >
              <Button variant="link" size="sm" className=" text-light">
                {/* <FontAwesomeIcon icon={faComment} /> */}
                <Image
                  className="mb-1"
                  src={CommentSvg}
                  alt=""
                  width="20px"
                  height="20px"
                />
                {' '}
                {post?.commentCount}
              </Button>

            </LinkContainer>

            {stripeInfo?.detailsSubmitted && stripeInfo?.chargesEnabled
              && stripeInfo?.payoutsEnabled && (

              <Button
                variant="link text-light"
                size="sm"
                className="mb-1"
                onClick={openTipModal}
              >
                {/* <FontAwesomeIcon icon={faCoins} /> */}
                <Image src={TipSvg} width="20px" height="20px" />
                {' '}
                <small>$</small>
                {((post as any)?.tipTotalCents ?? 0) / 100}
              </Button>
            ) }

            <WebShare
              title="Audiochills"
              text=""
              url={`https://audiochills.art/post/${post?.id}`}
            >
              <Button
                variant="link"
                size="sm"
                className="text-black-50"
              >

                {/* <FontAwesomeIcon icon={faShare} /> */}
                <Image src={ShareSvg} className="mb-1" width="20px" height="20px" />
              </Button>
            </WebShare>

          </div>

        </div>

      </Container>

    </div>
  );
};

export default PlayerFullScreen;
