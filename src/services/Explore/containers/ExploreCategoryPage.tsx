import React from 'react';
import { useTranslation } from 'react-i18next';
import {
  Button, Col, Container, Form, FormControl, InputGroup, Row, Spinner,
} from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { LinkContainer } from 'react-router-bootstrap';
import { useObservable } from 'react-use-observable';
import { Pending, social } from '../../../apis/social';
import { ProfileCardFeature } from './ProfileCardFeature';

export type ExploreCategoryPageProps = {
  categorySlug: string
};

export const ExploreCategoryPage: React.FC<ExploreCategoryPageProps> = ({ categorySlug }) => {
  const { t } = useTranslation('ExploreCategoryPage');
  useTranslation('ProfileCardFeature');
  const [categories] = useObservable(
    () => social.service('category').watch().find(), [],
  );
  const [category] = useObservable(
    () => social.service('category').watch().get(`slug:${categorySlug}`), [categorySlug],
  );

  return (
    <>
      {categories === null && <Pending />}
      <Container>
        <h1 className="h3 className mt-3">{`#${category?.name || ''}`}</h1>
        <Row noGutters className="flex-nowrap overflow-auto mb-3 pb-1 pt-1">
          {categories?.data.map(({ id, name, slug }, index) => (
            <Col xs="auto" key={id}>
              <LinkContainer to={`/explore/${slug}`}>
                <Button variant="secondary" size="sm" className={index > 0 ? 'ml-2' : ''}>
                  {`#${name}`}
                </Button>
              </LinkContainer>
            </Col>
          ))}
        </Row>
        <Form className="mb-3">
          <InputGroup>
            <InputGroup.Prepend>
              <InputGroup.Text><FontAwesomeIcon icon={faSearch} /></InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl type="text" placeholder={t('search')} />
          </InputGroup>
        </Form>
        <React.Suspense
          fallback={(
            <Row>
              <Col xs="auto" className="m-1 m-auto">
                <Spinner animation="border" />
              </Col>
            </Row>
          )}
        >
          {category === null && <Pending />}
          {!!category && (
          <Row className="justify-content-around">
            {category.profiles?.map((profile) => (
              <Col xs="auto" className="mb-2" key={profile.id}>
                <ProfileCardFeature
                  profileId={profile.id}
                  profile={profile}
                />
              </Col>
            ))}
          </Row>
          )}
        </React.Suspense>
      </Container>
    </>
  );
};
export default ExploreCategoryPage;
