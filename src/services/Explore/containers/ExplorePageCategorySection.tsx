import React from 'react';
import {
  Col, Row,
} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { CategoryAttributes } from '@audiochills/api-social/src/attributes';
import { useObservable } from 'react-use-observable';
import { of } from 'rxjs';
import { social, Pending } from '../../../apis/social';
import { ProfileCardFeature } from './ProfileCardFeature';

export type ExplorePageCategorySectionProps = {
  categoryId: string
  categoryOverride?: CategoryAttributes
};

export const ExplorePageCategorySection:
React.FC<ExplorePageCategorySectionProps> = ({ categoryId, categoryOverride }) => {
  const [category] = useObservable(
    () => (typeof categoryOverride !== 'undefined' ? of(categoryOverride) : social.service('category').watch().get(`${categoryId}`)),
    [categoryId, categoryOverride],
  );

  return (
    <>
      {category === null && <Pending />}
      <Link to={`/explore/${category?.slug}`}>
        <Row>
          <Col>
            <h2 className="h4 m-0">{`#${category?.name || ''}`}</h2>
          </Col>
          <Col xs="auto">
            See all
          </Col>
        </Row>
      </Link>
      <Row className="flex-nowrap overflow-auto mt-2 mb-2 pb-2">
        {category?.profiles?.map((profile) => (
          <Col xs="auto" className="pb-2" key={profile.id}>
            <ProfileCardFeature
              profileId={profile.id}
              profile={profile}
            />
          </Col>
        ))}
      </Row>
    </>
  );
};

export default ExplorePageCategorySection;
