import React from 'react';
import { useTranslation } from 'react-i18next';
import {
  Button, Col, Container, Form, FormControl, InputGroup, Row,
} from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import { LinkContainer } from 'react-router-bootstrap';
import { useObservable } from 'react-use-observable';
import { social, Pending } from '../../../apis/social';
import { ExplorePageCategorySection } from './ExplorePageCategorySection';
import LoadingPage from '../../Layout/containers/LoadingPage';

export type ExplorePageProps = {

};

export const ExplorePage: React.FC<ExplorePageProps> = () => {
  const { t } = useTranslation('ExplorePage');
  useTranslation('ExplorePageCategorySection');
  useTranslation('ProfileCardFeature');

  const [categories] = useObservable(
    () => social.service('category').watch().find(), [],
  );

  return (
    <>
      {categories === null && <Pending />}
      <Container>
        <h1 className="h3 text-center m-2">{t('heading')}</h1>
        <p className="text-muted text-center">{t('paragraph')}</p>
        <Form>
          <InputGroup className="mb-3">
            <InputGroup.Prepend>
              <InputGroup.Text><FontAwesomeIcon icon={faSearch} /></InputGroup.Text>
            </InputGroup.Prepend>
            <FormControl type="text" placeholder={t('search')} />
          </InputGroup>
        </Form>
        <Row noGutters className="justify-content-center mb-3">
          {categories && categories.data.map((category) => (
            <Col xs="auto" key={category.id}>
              <LinkContainer to={`/explore/${category.slug}`}>
                <Button style={{ backgroundColor: '#FBBCC4' }} variant="outline-primary" className="btn-sm m-1">
                  {`#${category.name}`}
                </Button>
              </LinkContainer>
            </Col>
          ))}
        </Row>
      </Container>
      <React.Suspense fallback={<LoadingPage />}>
        <Container>
          {categories && categories.data.map((category) => (
            <ExplorePageCategorySection
              categoryId={category.id}
              categoryOverride={category}
              key={category.id}
            />
          ))}
        </Container>
      </React.Suspense>
    </>
  );
};

export default ExplorePage;
