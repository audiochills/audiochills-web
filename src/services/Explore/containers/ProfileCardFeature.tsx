import React from 'react';
import { useTranslation, Trans } from 'react-i18next';
import { Card, Container } from 'react-bootstrap';
import { ProfileAttributes } from '@audiochills/api-social/src/attributes';
import { Link } from 'react-router-dom';
import { useObservable } from 'react-use-observable';
import social, { Pending } from '../../../apis/social';
import Avatar from '../../Profile/components/Avatar';
import AvatarPlaceHolder from '../images/avatar-placeholder.png';
import BgPlaceHolder from '../images/bg-placeholder.png';

export type ProfileCardFeatureProps = {
  profileId: string
  profile?: ProfileAttributes
};

export const ProfileCardFeature: React.FC<ProfileCardFeatureProps> = ({ profileId, profile }) => {
  const { t } = useTranslation('ProfileCardFeature');

  const [profileAvatar] = useObservable(
    () => social.service('profile-avatar').watch().get(`profileId:${profileId}`),
    [profileId],
  );

  const [profileBanner] = useObservable(
    () => social.service('profile-banner').watch().get(`profileId:${profileId}`),
    [profileId],
  );

  const [profileStats] = useObservable(
    () => social.service('profile').watch().get(profileId), [profile?.id],
  );

  return (
    <>
      {profileAvatar === null && <Pending />}
      {profileBanner === null && <Pending />}
      <Link to={`/profile/${profile?.username}`}>
        <Card style={{ height: '240px', width: '220px' }} className="overflow-hidden shadow-sm pb-2">
          <Card.Img
            variant="top"
            height="94px"
            src={(profileBanner as any)?.exists ? profileBanner?.downloadUrl : BgPlaceHolder}
            title={t('profileimage', { })}
          />
          <Container>
            <div style={{ display: 'inline-block', marginTop: 'calc(-96px / 2)' }}>
              <div style={{ display: 'inline-block', margin: 'calc(-4px)' }}>
                <Avatar width="72px" height="72px" src={(profileAvatar as any)?.exists ? profileAvatar?.downloadUrl : AvatarPlaceHolder} />
              </div>
            </div>
          </Container>
          <Container>
            <p className="font-weight-bold mb-0">
              {profile?.username}
            </p>
            <p className="mb-2">
              <small>
                <Trans i18nKey="followerCount" count={profile?.followerCount} t={t}>
                  <strong>{{ followerCount: profileStats?.followerCount }}</strong>
                  {' '}
                  Followers
                </Trans>

              </small>

            </p>
            <p>

              {
                `${profile?.aboutMessage}`?.length > 40
                  ? `${profile?.aboutMessage?.slice(0, 40)}...`
                  : profile?.aboutMessage
              }
            </p>
          </Container>
        </Card>
      </Link>
    </>
  );
};

export default ProfileCardFeature;
