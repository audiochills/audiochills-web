import React from 'react';
import { Route } from 'react-router-dom';

const ExplorePage = React.lazy(() => import('./containers/ExplorePage'));
const ExploreCategoryPage = React.lazy(() => import('./containers/ExploreCategoryPage'));

export default () => [
  <Route exact path="/explore" render={() => <ExplorePage />} />,
  <Route exact path="/explore/:categorySlug" render={({ match }) => <ExploreCategoryPage categorySlug={match.params.categorySlug!} />} />,
];
