import React, {
  useState, useCallback, useMemo,
} from 'react';
import { of } from 'rxjs';
import {
  Container, Button, Dropdown, DropdownButton,
} from 'react-bootstrap';
import { useObservable, useRetryableObservable } from 'react-use-observable';
import { useAsyncFn } from 'react-use';
import { Link } from 'react-router-dom';
import social, { Pending } from '../../../apis/social';
import Countries, { CountryType } from './Countries';
import { eventNames, logEvent } from '../../../analytics';

export const Onboarding:React.FC<{}> = () => {
  const [selectedCountry, setSelectedCountry] = useState<CountryType>();

  const [authentication] = useObservable(() => social.authentication.result(), []);
  const [stripeInfo, , , retryStripeInfo] = useRetryableObservable(
    () => (!authentication ? of(null) : social.service('stripe-info').watch({ idField: 'profileId' }).get(`profileId:${authentication.profile.id}`)),
    [authentication && authentication.profile.id],
  );

  const [profile] = useObservable(
    () => (!authentication ? of(null) : social.service('profile').watch().get(authentication.profile.id)), [authentication && authentication.profile.id],
  );

  // const a = async () => {
  //   if (!authentication) return;
  //   await (social.service('stripe-info') as any).patch(authentication.profile.id, {
  //     accountId: null, chargesEnabled: false,
  // countryIso: '', detailsSubmitted: false, payoutsEnabled: false,
  //   });
  // };

  // eslint-disable-next-line max-len
  const redirectToStripeUrl = useCallback(async () => {
    if (!authentication) return;
    if (stripeInfo && stripeInfo.url) {
      window.location.href = stripeInfo.url;
    } else {
      window.location.href = (await social.service('stripe-info').get(`profileId:${authentication.profile.id}`)).url ?? '';
    }
  }, [(authentication && authentication.profile.id)]);

  const [, createAccount] = useAsyncFn(
    async () => {
      if (!selectedCountry) return;
      if (!authentication) return;
      await social.service('stripe-info').patch(stripeInfo?.id ?? '_', { countryIso: selectedCountry.code, createAccount: true });
      retryStripeInfo();
      logEvent(eventNames.START_CREATOR_JOURNEY, {
        country: selectedCountry.code,
        userAudience: authentication.profile.followerCount,
        username: authentication?.profile?.username,
      });
    },
    [stripeInfo, selectedCountry, (authentication && authentication?.profile?.id)],
  );

  const processPayoutBtnEnabled = useMemo((): boolean => {
    if (!stripeInfo) return false;

    if (stripeInfo.payoutsEnabled
                  && stripeInfo.chargesEnabled
                  && stripeInfo.detailsSubmitted) {
      return false;
    }

    if (!stripeInfo.payoutsEnabled
                  && (stripeInfo.chargesEnabled === true
                      || stripeInfo.detailsSubmitted === true)) {
      return true;
    }

    return false;
  }, [stripeInfo]);

  if (!stripeInfo) return <Pending />;

  return (

    <Container>

      {!profile?.isCreatorAuthorized
        ? (
          <>
            <h3 className="mt-4">Request To Become A Creator</h3>
            {/* <Button
              className="pl-0 mt-5"
              variant="link"
              href="https://tally.so/r/mOzxMn"
              target="_blank"
            >
              Welcome! Let's get you started by filling up this form
            </Button> */}

            <iframe
              src="https://tally.so/embed/mOzxMn?hideTitle=1"
              width="100%"
              style={{ minHeight: '100vh' }}
              frameBorder="0"
              title="AudioChills Onboarding"
            />
          </>
        )
        : (
          <>
            {
        stripeInfo.payoutsEnabled && stripeInfo.payoutsEnabled && stripeInfo.payoutsEnabled
          ? (
            <>
              <h2 className="mt-4">Congrats! You are now a creator</h2>
              <br />
              <p>
                You can now receive tips and get paid for
                {' '}
                {' '}
                {/* eslint-disable-next-line react/no-unescaped-entities */}
                your content! Let's get you started by setting your
                <Link to="/profile-settings/subscription">
                  {' '}
                  {' '}
                  fan-subscriptions
                  {' '}
                </Link>
              </p>

            </>
          )
          : (
            <>
              <h4 className="mt-4">Step 1: Select your country</h4>
              <DropdownButton
                className="mt-3"
                menuAlign="left"
                title={selectedCountry?.country || stripeInfo?.countryIso || 'Select a country'}
                id="dropdown-menu-category"
                variant="outline-primary"
                disabled={stripeInfo?.accountId !== undefined
                  && stripeInfo?.accountId !== null}
              >
                {
        Countries.map((country, idx) => (
          <Dropdown.Item
            className="handStyle"
            key={country.code}
            eventKey={idx.toString()}
            onClick={() => setSelectedCountry(country)}
          >
            {country.country}
          </Dropdown.Item>

        ))
      }

              </DropdownButton>
              <Button
                className="mt-3"
                disabled={!((stripeInfo.accountId === undefined || stripeInfo.accountId === null))}
                onClick={() => createAccount()}
              >
                confirm
              </Button>

              <br />
              <small>
                Please select the country where your bank account is based
              </small>

              <h4 className="mt-5">Step 2: Banking details</h4>
              <Button
                className="mt-3"
                disabled={(stripeInfo.accountId !== undefined && stripeInfo.accountId !== null)
                  ? (stripeInfo.detailsSubmitted && stripeInfo.chargesEnabled)
                  : true}
                onClick={redirectToStripeUrl}
              >
                {' '}
                confirm
                {' '}

              </Button>

              <br />
              <small>
                You are almost there, we need some banking information to pay you
              </small>

              <h4 className="mt-5">Step 3: Verification</h4>

              <Button
                className="mt-3"
                disabled={!processPayoutBtnEnabled}
                onClick={redirectToStripeUrl}
              >
                {' '}
                confirm
                {' '}

              </Button>
              <br />
              <small>
                Congrats, you are in the last step, now all you need to
                do is confirming the information you submitted
                (This step might not be required for every creator)
              </small>
            </>
          )

      }
          </>
        )}

    </Container>
  );
};

export default Onboarding;
