import React from 'react';
import { Route } from 'react-router-dom';

const Onboarding = React.lazy(() => import('./containers/Onboarding'));

export default () => [
  <Route exact path="/become-creator" render={() => <Onboarding />} />,
];
