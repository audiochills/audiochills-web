import React from 'react';
import { render } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import { I18nextProvider } from 'react-i18next';
import Layout from '.';
import i18n, { initialized } from '../../i18n';

test('renders layout component', async () => {
  await initialized;
  const result = render(
    <React.Suspense fallback={null}>
      <I18nextProvider i18n={i18n}>
        <MemoryRouter>
          <Layout />
        </MemoryRouter>
      </I18nextProvider>
    </React.Suspense>,
  );
  expect(result).toMatchSnapshot();
});
