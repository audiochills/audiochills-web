import { Layout } from './containers/Layout';
import LoadingPage from './containers/LoadingPage';
import NotFoundPage from './containers/NotFoundPage';
import ErrorPage from './containers/ErrorPage';

export { LoadingPage, NotFoundPage, ErrorPage };
export default Layout;
