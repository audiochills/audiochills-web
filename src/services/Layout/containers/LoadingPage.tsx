import React from 'react';
import { Container, Spinner } from 'react-bootstrap';

const LoadingPage: React.FC<{}> = () => (
  <>
    <Container fluid className="position-absolute d-flex flex-column justify-content-center align-items-center vw-100 vh-100" style={{ left: 0, top: 0 }}>
      <Spinner animation="border" role="status" />
    </Container>
  </>
);

export default LoadingPage;
