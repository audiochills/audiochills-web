import React from 'react';
import { useTranslation } from 'react-i18next';
import {
  Dropdown, Nav, Navbar, NavItem, NavLink, Container, Button,
} from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faSignInAlt, faSignOutAlt,
} from '@fortawesome/free-solid-svg-icons';
import { LinkContainer } from 'react-router-bootstrap';
import { useObservable } from 'react-use-observable';
import { generatePath, useHistory } from 'react-router';
import { useAsyncFn } from 'react-use';
import { uuid } from 'uuidv4';
import { of } from 'rxjs';
import keycloak from '../../../keycloak';
import logo from '../images/logo.png';
import social, { Pending } from '../../../apis/social';
// import { ReactComponent as BellIcon } from '../images/bell.svg';
import { ReactComponent as PlusIcon } from '../images/plus.svg';
import { ReactComponent as ProfileIcon } from '../images/profile.svg';
import { ReactComponent as HomeIcon } from '../images/home.svg';
import { ReactComponent as AccountSettingsSvg } from '../images/profileSettings.svg';
import { ReactComponent as PayoutsSvg } from '../images/payouts.svg';
import { ReactComponent as MonetizationSvg } from '../images/monetizationPlans.svg';
// import { ReactComponent as NotificationsSvg } from '../images/notification.svg';
import { ReactComponent as PrivacySvg } from '../images/privacyDeclaration.svg';
import { ReactComponent as CopyrightsSvg } from '../images/copyrights.svg';
import { ReactComponent as MenuSvg } from '../images/menu.svg';
import { ReactComponent as SearchSvg } from '../images/search.svg';
import { eventNames, logEvent } from '../../../analytics';

export type LayoutProps = {
};

const itemStyle = {
  borderRadius: '20px', opacity: 0.7, padding: '11px 10px',
};

// eslint-disable-next-line react/jsx-props-no-spreading
const LiNavItem: React.FC<object> = ({ children, ...props }) => <NavItem as="li" {...props}>{children}</NavItem>;

export const Layout: React.FC<LayoutProps> = ({ children }) => {
  const { t } = useTranslation('Layout');

  const [authentication] = useObservable(() => social.authentication.result(), []);

  const history = useHistory();

  const [initPostState, initPost] = useAsyncFn(async () => {
    const newPostId = uuid();
    const post = await (social.service('post') as any).create({
      id: newPostId,
      postAudio: {
        postId: newPostId,
      },
      postImage: {
        postId: newPostId,
      },
    });
    // if (Array.isArray(post)) return;
    logEvent(eventNames.STARTED_POST_CREATION, { postId: post.id });
    history.push(`/post/general/${post.id}`);
  }, []);

  const [profile] = useObservable(
    () => (!authentication ? of(null) : social.service('profile').watch().get(authentication?.profile?.id)),
    [authentication && authentication?.profile?.id],
  );

  if (authentication === null) return <Pending />;
  return (
    <>
      {authentication === null && <Pending />}

      <div
        className="d-flex justify-content-center"
        style={{
          left: 0, position: 'fixed', right: 0, top: 0, zIndex: 999,
        }}
      >

        <Navbar
          variant="light"
          bg="white"
          expand
          className="justify-content-between shadow-sm"
          style={{ maxWidth: '600px', width: '100%', zIndex: 1 }}
        >
          <LinkContainer to="/" exact>
            <Navbar.Brand><img src={logo} alt={t('navbarBrand')} /></Navbar.Brand>
          </LinkContainer>
          <Nav
            as="ul"
            className="flex-row"
          >
            <LiNavItem>
              <LinkContainer to="/explore" exact>
                <NavLink>
                  <SearchSvg width="24px" height="24px" className="mr-3" />
                </NavLink>
              </LinkContainer>
            </LiNavItem>
            <Dropdown
              as={LiNavItem}
              drop="down"
            >
              <Dropdown.Toggle as={NavLink} bsPrefix="--not-existing">
                <MenuSvg width="24px" height="24px" />
                {/* <FontAwesomeIcon icon={faBars} /> */}
                {/* <span className="d-none d-sm-inline">
                {' '}
                {t('more')}
              </span> */}
              </Dropdown.Toggle>

              <Dropdown.Menu
                align="right"
                style={{ maxWidth: '370px', width: '90vw' }}
                className="px-2"
              >
                {/*
              <div className="w-100 d-flex justify-content-end mb-4">
                <Button variant="link">
                  <CloseSvg />
                </Button>

              </div> */}

                {authentication && profile && (
                <LinkContainer
                  to={generatePath('/profile/:profileUsername', { profileUsername: profile.username })}
                  exact
                  className="mb-2"
                  style={itemStyle}
                >
                  <Dropdown.Item>
                    <ProfileIcon width="22px" height="22px" />
                    {' '}
                    {t('My Profile')}
                  </Dropdown.Item>
                </LinkContainer>
                )}

                {authentication && (
                <LinkContainer
                  to={generatePath('/profile-settings')}
                  exact
                  className="mb-2"
                  style={itemStyle}
                >
                  <Dropdown.Item>
                    <AccountSettingsSvg width="22px" height="22px" />
                    {' '}
                    {t('Profile Settings')}
                  </Dropdown.Item>
                </LinkContainer>
                )}

                {authentication && (
                <LinkContainer
                  to="#"
                  exact
                  className="mb-2"
                  style={itemStyle}
                  onClick={async () => {
                    if (authentication) {
                      const localStripeInfo = await social.service('stripe-info').get(`profileId:${authentication?.profile?.id}`);
                      window.location.href = (localStripeInfo as any)?.customerPortal;
                    }
                  }}
                >
                  <Dropdown.Item>
                    <MonetizationSvg width="22px" height="22px" />
                    {' '}
                    {t('ManageMySubscriptions')}
                  </Dropdown.Item>
                </LinkContainer>
                )}
                {authentication && (
                <LinkContainer
                  to={generatePath('/profile-settings/subscription')}
                  exact
                  className="mb-2"
                  style={itemStyle}
                >
                  <Dropdown.Item>
                    <MonetizationSvg width="22px" height="22px" />
                    {' '}
                    {t('Monetization')}
                  </Dropdown.Item>
                </LinkContainer>
                )}
                {authentication && (
                <Dropdown.Item
                  onClick={() => keycloak && keycloak.accountManagement()}
                  className="mb-2"
                  style={itemStyle}
                >
                  <AccountSettingsSvg width="22px" height="22px" />
                  {' '}
                  {t('Account Settings')}
                </Dropdown.Item>
                )}
                {authentication && (
                <LinkContainer
                  to={generatePath('/become-creator')}
                  exact
                  className="mb-2"
                  style={itemStyle}
                >
                  <Dropdown.Item>
                    <PayoutsSvg width="22px" height="22px" />
                    {' '}
                    {t('Payouts')}
                  </Dropdown.Item>
                </LinkContainer>
                )}
                {/* {authentication && (
                <LinkContainer
                  to={generatePath('/notifications')}
                  exact
                  className="mb-2"
                  style={itemStyle}
                >
                  <Dropdown.Item>
                    <NotificationsSvg width="22px" height="22px" />
                    {' '}
                    {t('Notifications')}
                  </Dropdown.Item>
                </LinkContainer>
                )} */}
                {authentication && (
                <LinkContainer
                  to="/privacy-policy"
                  className="mb-2"
                  style={itemStyle}
                  exact
                >
                  <Dropdown.Item>
                    <PrivacySvg width="22px" height="22px" />
                    {' '}
                    {t('PrivacyPolicy')}
                  </Dropdown.Item>
                </LinkContainer>
                )}
                {authentication && (
                <LinkContainer
                  to="/terms-of-service"
                  exact
                  className="mb-2"
                  style={itemStyle}

                >
                  <Dropdown.Item>
                    <CopyrightsSvg width="22px" height="22px" />
                    {' '}
                    {t('TermsOfService')}
                  </Dropdown.Item>
                </LinkContainer>
                )}

                {authentication && (
                <LinkContainer
                  to="/copyright-licenses"
                  exact
                  className="mb-2"
                  style={itemStyle}
                >
                  <Dropdown.Item>
                    <CopyrightsSvg width="22px" height="22px" />
                    {' '}
                    {t('Copyright')}
                  </Dropdown.Item>
                </LinkContainer>
                )}

                {/* {authentication && (
              <LinkContainer
                to={generatePath('/nsfw-rules')}
                exact
                className="mb-2"
                style={itemStyle}
              >
                <Dropdown.Item>
                  <NsfwRulesSvg width="22px" height="22px" />
                  {' '}
                  {t('NSFW rules')}
                </Dropdown.Item>
              </LinkContainer>
              )} */}
                {authentication && (
                <Dropdown.Item
                  onClick={() => keycloak && keycloak.logout()}
                  className="mb-2"
                  style={itemStyle}
                >
                  <FontAwesomeIcon icon={faSignOutAlt} />
                  {' '}
                  {t('logout')}
                </Dropdown.Item>
                )}
                {!authentication && (
                <Dropdown.Item
                  onClick={() => keycloak && keycloak.login()}
                  className="mb-2"
                  style={itemStyle}
                >
                  <FontAwesomeIcon icon={faSignInAlt} />
                  {' '}
                  {t('login')}
                </Dropdown.Item>
                )}

              </Dropdown.Menu>
            </Dropdown>
          </Nav>
        </Navbar>
      </div>

      <div style={{ height: '58px' }} />

      {children}

      <div style={{ height: '64px' }} />

      <div
        className="d-flex justify-content-center"
        style={{
          bottom: 0, left: 0, position: 'fixed', right: 0,
        }}
      >
        <Navbar className="border" bg="light" variant="light" style={{ bottom: 0, maxWidth: '600px', width: '100%' }}>
          <Container>
            <LinkContainer to="/">
              <Button variant="link">
                <p className="m-0"><HomeIcon /></p>
              </Button>
            </LinkContainer>
            {authentication && (
            <Button variant="link" onClick={initPost} disabled={initPostState.loading}>
              <p className="m-0"><PlusIcon /></p>
            </Button>
            )}
            {!authentication && (
            <Button
              variant="link"
              onClick={() => keycloak && keycloak.login()}
            >

              <p className="m-0"><PlusIcon /></p>
            </Button>
            )}
            {/* <LinkContainer to="/notifications">
              <Button variant="link">
                <p className="m-0"><BellIcon /></p>
              </Button>
            </LinkContainer> */}
            {authentication && (
            <LinkContainer to={generatePath('/profile/:profileUsername', { profileUsername: profile?.username ?? '_' })}>
              <Button variant="link">

                <p className="m-0"><ProfileIcon /></p>
              </Button>
            </LinkContainer>
            )}
            {!authentication && (
            <Button
              variant="link"
              onClick={() => keycloak && keycloak.login()}
            >
              <p className="m-0"><ProfileIcon /></p>

            </Button>
            )}

          </Container>
        </Navbar>

      </div>

    </>
  );
};

export default Layout;
