import { faBan } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import { Button, Container } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';
import { LinkContainer } from 'react-router-bootstrap';

const NotFoundPage: React.FC<{}> = () => {
  const { t } = useTranslation('NotFoundPage');
  return (
    <>
      <Container fluid className="position-absolute d-flex flex-column justify-content-center align-items-center w-100 h-100" style={{ top: 0 }}>
        <FontAwesomeIcon icon={faBan} />
        <span className="m-2">{t('notFound')}</span>
        <p>
          <LinkContainer to="/">
            <Button>{t('back')}</Button>
          </LinkContainer>
        </p>
      </Container>
    </>
  );
};

export default NotFoundPage;
