import { faBan } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React from 'react';
import { Alert, Button, Container } from 'react-bootstrap';
import { useTranslation } from 'react-i18next';

type ErrorPageProps = {
  error: any
};
const ErrorPage: React.FC<ErrorPageProps> = ({ error }) => {
  const { t } = useTranslation('ErrorPage');
  return (
    <>
      <Container fluid className="position-absolute d-flex flex-column justify-content-center align-items-center w-100 h-100" style={{ left: 0, top: 0 }}>
        <FontAwesomeIcon icon={faBan} />
        <span className="m-2">{t('error', { error })}</span>
        <p>
          <Alert variant="danger" style={{ maxWidth: '600px' }}>
            {`${error}`}
          </Alert>
        </p>
        <p>
          <Button onClick={() => window.location.reload()}>{t('reload', { error })}</Button>
        </p>
      </Container>
    </>
  );
};

export default ErrorPage;
