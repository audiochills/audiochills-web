import React from 'react';
import { Route } from 'react-router-dom';
import TopBar from '../PostCreation/containers/TopBar';
import NotificationsPage from './containers/NotificationsPage';

export default () => [
  <Route
    exact
    path="/notifications"
    render={() => (
      <TopBar progress={0} title="Notifications" navigateBackTo="/">
        {' '}
        <NotificationsPage />
        {' '}
      </TopBar>
    )}
  />,
];
