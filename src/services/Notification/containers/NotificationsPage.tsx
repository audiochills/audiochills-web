import { useObservable } from 'react-use-observable';
import React from 'react';
import {
  Button,
  Col, Container, Row,
} from 'react-bootstrap';
import Moment from 'react-moment';
import { LinkContainer } from 'react-router-bootstrap';
import { of } from 'rxjs';
import moment from 'moment';
import social, { Pending } from '../../../apis/social';
import avatar from '../images/acLogo.svg';
import keycloak from '../../../keycloak';

export const NotificationPage: React.FC<{}> = () => {
  const [authentication] = useObservable(() => social.authentication.result(), []);

  const [notifications, notificationsError] = useObservable(
    () => (authentication
      ? of(null)
      : social.service('notification').watch().find()),
    [],
  );

  if (notificationsError) throw notificationsError;
  if (authentication === false) keycloak.login();

  return (
    <>
      {authentication && notifications === null && <Pending />}
      {notifications && !notifications.data.length && (
        <Container className="mt-5">
          {/* <h5>{t('emptyParagraph')}</h5> */}
          <h5>Explore new content and check your activities here</h5>
          <LinkContainer className="mt-5" to="/explore" exact>
            {/* <Button>{t('emptyExploreButtonLabel')}</Button> */}
            <Button>Explore content</Button>
          </LinkContainer>
        </Container>
      )}
      {notifications && !!notifications.data.length && notifications.data.map((notification) => (
        <Container className="px-4" style={{ maxWidth: '800px' }}>
          <Row className="mt-3">
            <Col xs={3} md={2}>
              <Row>
                <Col className="px-3">

                  <Col xs={12} className="d-flex justify-content-center">
                    <img
                      width="24"
                      height="24"
                      alt={notification?.notified?.name || 'author'}
                      src={
                        (notification?.notified?.profileAvatar as any)?.exists
                          ? notification?.notified?.profileAvatar?.downloadUrl
                          : avatar
                      }
                      className="rounded-circle"
                    />
                  </Col>
                  <Col xs={12} className="d-flex justify-content-center p-0">
                    <small
                      className="mt-1 pl-0"
                      style={{ minWidth: '76px' }}
                    >
                      <Moment fromNow ago date={moment(notification.createdAt)} />
                      {' '}
                      ago
                    </small>
                  </Col>

                </Col>
              </Row>
            </Col>

            <Col className="pl-2 pt-0">
              <Row>
                <Col>
                  <strong>{notification?.notified?.username || ''}</strong>
                </Col>
              </Row>
              <Row>
                <Col>
                  <p>{notification.message}</p>
                </Col>
              </Row>
            </Col>
          </Row>

          <Row>
            <Col className="px-2">
              <hr className="p-0 mt-2 mx-0" />
            </Col>
          </Row>
        </Container>
      ))}
    </>
  );
};

export default NotificationPage;
