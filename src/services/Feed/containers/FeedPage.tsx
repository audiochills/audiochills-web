import React from 'react';
import { useTranslation } from 'react-i18next';
import { Button, Container } from 'react-bootstrap';
import { paramsForServer } from 'feathers-hooks-common';
import { useObservable } from 'react-use-observable';
import { Link } from 'react-router-dom';
import { of } from 'rxjs';
import { social, Pending } from '../../../apis/social';
import PostItem from '../../Profile/containers/PostItem';
import { ReactComponent as EarSvg } from '../images/ear.svg';
import keycloak from '../../../keycloak';

export type FeedPageProps = {

};

export const FeedPage: React.FC<FeedPageProps> = () => {
  const { t } = useTranslation('FeedPage');
  useTranslation('PostItem');

  const [authentication] = useObservable(() => social.authentication.result(), []);

  const [posts, postsError] = useObservable(
    () => (!authentication
      ? of(null)
      : social.service('post').watch().find(paramsForServer({ personal: true, query: { isPublished: true } }))),
    [authentication],
  );

  if (postsError) throw postsError;
  if (authentication === false) keycloak.login();

  return (
    <>
      {authentication && posts === null && <Pending />}
      {posts && !posts.data.length && (
      <>
        <Container className="d-flex flex-column justify-content-center align-items-center" style={{ marginTop: '200px' }}>
          <p>
            <EarSvg />
          </p>
          <strong>{t('emptyParagraph')}</strong>
          {' '}

          <Link to="/explore">
            <Button className="mt-4 px-5 py-2">{t('emptyExploreButtonLabel')}</Button>
          </Link>

        </Container>
      </>
      )}
      {posts && !!posts.data.length && posts.data.map((post) => (
        <PostItem postId={post.id} postOverride={post} key={post.id} />
      ))}
      <p style={{ marginBottom: '200px' }} />

    </>
  );
};

export default FeedPage;
