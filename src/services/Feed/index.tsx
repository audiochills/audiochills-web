import React from 'react';
import { Route } from 'react-router';

const FeedPage = React.lazy(() => import('./containers/FeedPage'));

export default () => [
  <Route exact path="/" render={() => <FeedPage />} />,
];
